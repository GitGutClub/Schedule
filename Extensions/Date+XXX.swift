//
//  Date+XXX.swift
//  Schedule
//
//  Created by MacOS on 02.07.2019.
//  Copyright © 2019 GZ. All rights reserved.
//

import Foundation

typealias DateCellType = (dateNumber: String, day: String, date: Date)

extension Date {
    
    func takeDayNumber() -> Int{
        let dateEnd = self
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "ru_RU")
        formatter.dateFormat = "dd"
        let date = formatter.string(from: dateEnd as Date)
        let numberDay = Int(date)
        return numberDay!
    }
    
    func takeMonthNumber() -> Int{
        let dateEnd = self
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "ru_RU")
        formatter.dateFormat = "MM"
        let date = formatter.string(from: dateEnd as Date)
        let numberMonth = Int(date)
        return numberMonth!
    }
    
    static func changeDays(date: Date, by days: Int) -> Date {        
        let date: Date = Calendar.current.date(byAdding: .day, value: days, to: date)!
        return date
    }
    
    static func changeMounth(date: Date, by value: Int) -> Date {
        let date: Date = Calendar.current.date(byAdding: .month, value: value, to: date)!
        return date
    }
    
    static func nowDateToString() -> String {
        let dateEnd = Date()
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "ru_RU")
        formatter.dateFormat = "yyyy.MM.dd"
        let date = formatter.string(from: dateEnd as Date)
        return date
    }
    
    static func dateFromString(dateStr: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "ru_RU")
        dateFormatter.dateFormat = "yyyy.MM.dd"
        let date = dateFormatter.date(from: dateStr)
        return date
    }
    
    static func stringFromDate(date: Date) -> String {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "ru_RU")
        formatter.dateFormat = "yyyy.MM.dd"
        let dateForm = formatter.string(from: date as Date)
        return dateForm
    }
    
    static func dayStringFromDate(date: Date) -> String {
        let formatter = DateFormatter()
        
        formatter.dateFormat = "EE"
        formatter.locale = Locale(identifier: "ru_RU")
        let dateForm = formatter.string(from: date as Date)
        
        let index1 = dateForm.index(dateForm.endIndex, offsetBy: -(dateForm.count - 2))
        
        let substring1 = dateForm[..<index1]
        
        return String(substring1)
    }
    
    static func monthNameStringFromDate(date: Date) -> String {
        let formatter = DateFormatter()
        
        formatter.dateFormat = "LLLL"
        formatter.locale = Locale(identifier: "ru_RU")
        let dateForm = formatter.string(from: date as Date)
        
        return dateForm
    }
    
    static func daysFromDate(date: String) -> [DateCellType] {
        
        var arrayDate = [DateCellType]()
        
        let dateReference = Date.dateFromString(dateStr: date)
        
        let calendar = Calendar.current
        
        let dateComponents = DateComponents(year: calendar.component(.year, from: dateReference!), month: calendar.component(.month, from: dateReference!))
        
        var date = calendar.date(from: dateComponents)!
        
        let range = calendar.range(of: .day, in: .month, for: date)!
        let numDays = range.count
       
        for i in 1...numDays {
            arrayDate.append((dateNumber: String(i), day: Date.dayStringFromDate(date: date), date: date))
            date = Date.changeDays(date: date, by: +1)
        }
       
        return arrayDate
    }
}
