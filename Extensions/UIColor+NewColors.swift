//
//  UIColor+NewColors.swift
//  Schedule
//
//  Created by MacOS on 02.08.2019.
//  Copyright © 2019 GZ. All rights reserved.
//

import Foundation
import UIKit

class ImageWithoutRender: UIImage {
    override func withRenderingMode(_ renderingMode: UIImage.RenderingMode) -> UIImage {
        return self
    }
}

extension UIColor {
    
    static func swipeActionBackgroungColor() -> UIColor {
        return UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1.0)
    }
    
    static func notValidColor() -> UIColor{
        return UIColor(red: 214/255, green: 48/255, blue: 49/255, alpha: 0.9)
    }
    
    static func studentSwitchColor() -> UIColor {
       return UIColor(red: 0.77, green: 0.77, blue: 0.77, alpha: 1)
    }
    
    convenience init?(hex: String) {
        var hexSanitized = hex.trimmingCharacters(in: .whitespacesAndNewlines)
        hexSanitized = hexSanitized.replacingOccurrences(of: "#", with: "")
        
        var rgb: UInt32 = 0
        
        var r: CGFloat = 0.0
        var g: CGFloat = 0.0
        var b: CGFloat = 0.0
        var a: CGFloat = 1.0
        
        let length = hexSanitized.count
        
        guard Scanner(string: hexSanitized).scanHexInt32(&rgb) else { return nil }
        
        if length == 6 {
            r = CGFloat((rgb & 0xFF0000) >> 16) / 255.0
            g = CGFloat((rgb & 0x00FF00) >> 8) / 255.0
            b = CGFloat(rgb & 0x0000FF) / 255.0
            
        } else if length == 8 {
            r = CGFloat((rgb & 0xFF000000) >> 24) / 255.0
            g = CGFloat((rgb & 0x00FF0000) >> 16) / 255.0
            b = CGFloat((rgb & 0x0000FF00) >> 8) / 255.0
            a = CGFloat(rgb & 0x000000FF) / 255.0
            
        } else {
            return nil
        }
        
        self.init(red: r, green: g, blue: b, alpha: a)
    }
}

typealias Color = (hex: String, color: UIColor)

class GroupColors {
   var color1: Color = (hex: "#FF6E6E", color: UIColor(red: 1, green: 0.43, blue: 0.43, alpha: 1))
   var color2: Color = (hex: "#2E86DE", color: UIColor(red: 0.18, green: 0.53, blue: 0.87, alpha: 1))
   var color3: Color = (hex: "#FECA57", color: UIColor(red: 1, green: 0.79, blue: 0.34, alpha: 1))
   var color4: Color = (hex: "#16B877", color: UIColor(red: 0.09, green: 0.72, blue: 0.47, alpha: 1))
   lazy var colorsArray = [color1, color2, color3, color4]
}
