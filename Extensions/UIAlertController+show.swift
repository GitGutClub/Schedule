//
//  UIAlertController+show.swift
//  Schedule
//
//  Created by MacOS on 02.07.2019.
//  Copyright © 2019 GZ. All rights reserved.
//

import Foundation
import UIKit

extension UIAlertController {
    
    static func presentAlert(on: UIViewController, errorName: String) {
        let alert = UIAlertController(title: "Ошибка!",
                                      message: "\(errorName)",
            preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(
            title: "OK",
            style: .default,
            handler: nil
        ))
        on.present(alert, animated: true, completion: nil)
    }
    
    static func presentAlert(on: UIViewController, messegeText: String, complition: @escaping () -> ()) {
        let alert = UIAlertController(title: "Внимание!",
            message: "\(messegeText)",
            preferredStyle: .alert)
        
        alert.addAction(UIAlertAction (
            title: "OK",
            style: .default,
            handler: { (alert) in complition() }
        ))
        on.present(alert, animated: true, completion: nil)
    }
    
    static func presentActionSheet(on: UIViewController, name: String,  buttons: [UIAlertAction]) {
        let actionSheet = UIAlertController(title: name, message: "", preferredStyle: .actionSheet)
        
        for action in buttons {
            actionSheet.addAction(action)
        }
        actionSheet.addAction(UIAlertAction(title: "Закрыть", style: .cancel, handler: { (alert) in
            on.dismiss(animated: true, completion: nil)
        }))
        // Present action sheet.
        on.present(actionSheet, animated: true)
    }
}
