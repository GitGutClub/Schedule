//
//  UIViewController+Keyboard.swift
//  Schedule
//
//  Created by MacOS on 02.07.2019.
//  Copyright © 2019 GZ. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func hideKeyboardWhenSwipeDown() {
        let swipeDown = UISwipeGestureRecognizer(
            target: self,
            action: #selector(hideKeyboard)
        )
        swipeDown.direction = UISwipeGestureRecognizer.Direction.down
        view.addGestureRecognizer(swipeDown)
    }
    
    func hideKeyboardWhenTappedAround() {
        let tapRecognizerGesture = UITapGestureRecognizer(
            target: self,
            action: #selector(hideKeyboard)
        )
        view.addGestureRecognizer(tapRecognizerGesture)
    }
    
    @objc func hideKeyboard() {
        view.endEditing(true)
    }    
}
