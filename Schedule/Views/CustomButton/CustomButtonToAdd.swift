//
//  CustomButtonToAdd.swift
//  Schedule
//
//  Created by MacOS on 05.08.2019.
//  Copyright © 2019 GZ. All rights reserved.
//

import UIKit

protocol CustomButtonDelegate {
    func buttonPressed()
}

class CustomButtonToAdd: BasicXIBView { 
    var delegate: CustomButtonDelegate?
    
    @IBOutlet var button: UIButton!
    
    public func setButtonTitle(title: String) {
        self.button.setTitle("           \(title)", for: .normal)
    }
    
    @IBAction func buttonPressed(_ sender: Any) {
        delegate?.buttonPressed()
    }
}
