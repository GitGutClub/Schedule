//
//  BaseViewForCustomComponent.swift
//  Schedule
//
//  Created by MacOS on 21.05.2019.
//  Copyright © 2019 GZ. All rights reserved.
//

import UIKit

class BasicXIBView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup() {
        let nibName = NSStringFromClass(type(of: self)).components(separatedBy: ".").last!
        guard let view = Bundle.main.loadNibNamed(nibName, owner: self, options: nil)?[0] as? UIView else {
            fatalError("Must not be nil")
        }
        
        view.translatesAutoresizingMaskIntoConstraints = false;
        self.addSubview(view)
        
        let constraints = [
            view.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            view.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            view.topAnchor.constraint(equalTo: self.topAnchor),
            view.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ]
        self.addConstraints(constraints)
    }
}
