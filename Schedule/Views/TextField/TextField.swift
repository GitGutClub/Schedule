//
//  ViewController.swift
//  Schedule
//
//  Created by MacOS on 20.05.2019.
//  Copyright © 2019 GZ. All rights reserved.
//

import UIKit

class TextField: BasicXIBView, UITextFieldDelegate {
   
    @IBOutlet var stackView: UIStackView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var textField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textField.setBottomBorder()
        textField.delegate = self
    }
    
    func setValid(_ valid: Bool) {
        if valid == false {
        nameLabel.textColor = UIColor.notValidColor()
    }
}
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        nameLabel.textColor = UIColor.darkText
        
        return true
    }  
}

