//
//  NumberPairTextField.swift
//  Schedule
//
//  Created by MacOS on 22.06.2019.
//  Copyright © 2019 GZ. All rights reserved.
//

import Foundation
import UIKit

class NumberPairTextField: TextField {
    
    override func xibSetup() {
        let nibName = NSStringFromClass(type(of: TextField())).components(separatedBy: ".").last!
        guard let view = Bundle.main.loadNibNamed(nibName, owner: self, options: nil)?[0] as? UIView else {
            fatalError("Must not be nil")
        }
        
        view.translatesAutoresizingMaskIntoConstraints = false;
        self.addSubview(view)
        
        let constraints = [
            view.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            view.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            view.topAnchor.constraint(equalTo: self.topAnchor),
            view.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ]
        self.addConstraints(constraints)
    }
    
    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        nameLabel.textColor = UIColor.darkText
        let invalidCharacters = CharacterSet(charactersIn: "123456789").inverted
        let maxLength = 1
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength && string.rangeOfCharacter(from: invalidCharacters) == nil
    }
}
