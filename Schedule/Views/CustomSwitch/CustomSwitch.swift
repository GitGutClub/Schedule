//
//  UIViewViewController.swift
//  Schedule
//
//  Created by MacOS on 30.07.2019.
//  Copyright © 2019 GZ. All rights reserved.
//

import UIKit

class CustomSwitch: BasicXIBView {
    
    @IBOutlet var customSwitch: UISwitch!
    @IBOutlet var offLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        customSwitch.tintColor = UIColor.studentSwitchColor()
        offLabel.isHidden = customSwitch.isOn
    }
}
