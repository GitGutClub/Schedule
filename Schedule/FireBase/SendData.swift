//
//  SendData.swift
//  DataBase1
//
//  Created by MacOS on 08.04.2019.
//  Copyright © 2019 GZ. All rights reserved.
//

import Foundation
import Firebase

class SendData {
    
    static func sendUserData(user: UserEntity, uid: String,  completion: @escaping (() -> Void)) {
        
        let pointsDB = FirebaseAPI.referenceWithUser.ref.child(uid).child("userData")
        
        let data = try? JSONEncoder().encode(user)
        
        guard let pointDictionary = try? ((JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any])!) else {
            return
        }
        
        pointsDB.setValue(pointDictionary) { (error, reference) in
            if error != nil {
                print(error!)
            } else {
                print("Message saved successfully!")
                completion()
            }
        }
    }
    
    static func sendSchedule(schedule: ScheduleEntity, completion: @escaping (() -> Void)) {
        
        let pointsDB = FirebaseAPI.referenceWithUserID.ref.child("schedule")

        let data = try? JSONEncoder().encode(schedule)
        
        guard let pointDictionary = try? ((JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any])!) else {            
            return
        }
        
        pointsDB.childByAutoId().setValue(pointDictionary) { (error, reference) in
            if error != nil {
                print(error!)
            } else {
                print("Message saved successfully!")
                completion()
            }
        }
    }
    
    static func sendAttendance(students: [StudentEntity],scheduleKey: String, completion: @escaping (() -> Void)) {
        for i in 0...students.count - 1 {
        let pointsDB = FirebaseAPI.referenceWithUserID.ref.child("attendance").child(scheduleKey).child("students")
        
        let data = try? JSONEncoder().encode(students[i])

        guard let pointDictionary = try! JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any] else {
            print("fail")
            return
        }
        
        pointsDB.childByAutoId().setValue(pointDictionary) { (error, reference) in
            if error != nil {
                print(error!)
            } else {
                print("Message saved successfully!")
                completion()
            }
        }
    }
}
    
    static func sendGroup(group: GroupEntity, completion: @escaping (() -> Void)) {
        
        let pointsDB = FirebaseAPI.referenceWithUserID.ref.child("groups")
        let data = try? JSONEncoder().encode(group)
        
        guard let pointDictionary = try? ((JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any])!) else {
            return
        }
        pointsDB.childByAutoId().setValue(pointDictionary) { (error, reference) in
            if error != nil {
                print(error!)
            } else {
                print("Message saved successfully!")
                completion()
            }
        }
    }
    
    static func sendDiscipline(name: String, completion: @escaping (() -> Void)) {
        
        let pointsDB = FirebaseAPI.referenceWithUserID.ref.child("discipline")
        
        let pointDictionary = ["nameDiscipline": name]
        pointsDB.childByAutoId().setValue(pointDictionary) { (error, reference) in
            if error != nil {
                print(error!)
            } else {
                print("Message saved successfully!")
                completion()
            }
        }
    }
    
    static func sendNewStudent(_ student: StudentEntity, keyGroup: String, completion: @escaping (() -> Void)) {
        
        let pointsDB = FirebaseAPI.referenceWithUserID.ref.child("groups").child(keyGroup).child("students")
        
        let data = try? JSONEncoder().encode(student)
        
        guard let pointDictionary = try? ((JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any])!) else {
            return
        }
        
        pointsDB.childByAutoId().setValue(pointDictionary) { (error, reference) in
            if error != nil {
                print(error!)
            } else {
                print("Message saved successfully!")
                
                ScheduleDatabase.groups { (data) in
                    
                    if data == nil {
                        ScheduleDatabase.groupsArray = [GroupEntity]()
                        return
                    }
                    completion()
                }
            }
        }
    }
}
