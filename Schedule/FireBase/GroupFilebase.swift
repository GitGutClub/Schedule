//
//  StorageDatabase.swift
//  Schedule
//
//  Created by MacOS on 04.06.2019.
//  Copyright © 2019 GZ. All rights reserved.
//

import Foundation
import Firebase
import FirebaseStorage

class GroupFilebase {
    
    static func uploadStudentProfileImage(_ image: UIImage, name: String,  completion: @escaping (URL?) -> Void) {
       
        let reference = Storage.storage().reference().child("studentsProfileImage").child(name)
        
        guard let imageData = image.jpegData(compressionQuality: 1) else {
            return completion(nil)
        }
      
        reference.putData(imageData, metadata: nil, completion: { (metadata, error) in
            if let error = error {
                assertionFailure(error.localizedDescription)
                return completion(nil)
            }
         
            reference.downloadURL(completion: { (url, error) in
                if let error = error {
                    assertionFailure(error.localizedDescription)
                    return completion(nil)
                }
                completion(url)
            })
        })
    }
}


