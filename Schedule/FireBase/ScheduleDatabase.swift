//
//  ScheduleDatabase.swift
//  Schedule
//
//  Created by MacOS on 25.05.2019.
//  Copyright © 2019 GZ. All rights reserved.
//

import Foundation
import Firebase
import FirebaseAuth

enum FirebaseAPI {
    case reference
    case referenceWithUser
    case referenceWithUserID
    
    var ref: DatabaseReference {
        switch self {
        case .reference:
            return Database.database().reference().child("point")
        case .referenceWithUser:
            return FirebaseAPI.reference.ref.child("users")
        case .referenceWithUserID:
            return FirebaseAPI.referenceWithUser.ref.child((Auth.auth().currentUser?.uid)!)
        }
    }
}

class ScheduleDatabase {
    
    static var currentUser = UserEntity()
    static var timeArray = [ScheduleTimeSetEntity]()
    static var scheduleArray = [ScheduleEntity]()
    static var typePairArray = [PairTypeEntity]()
    static var groupsArray = [GroupEntity]()
    static var typeGroupArray = [GroupTypeEntity]()
    static var attendanceArray = [AttendanceEntity]()
    
    static func pairsByDate(date: Date, completion: @escaping (([ScheduleEntity]?) -> Void)) {
        let dbRef: DatabaseReference = FirebaseAPI.referenceWithUserID.ref.child("schedule")
        
        let query: DatabaseQuery = dbRef.queryOrdered(byChild: "date").queryEqual(toValue: Date.stringFromDate(date: date))
        query.observeSingleEvent(of: .value) { (snapshot) in
            guard let schedule: [String: Any?] = snapshot.value as? [String: Any?] else {
                completion(nil)
                return
            }
            
            var items: [ScheduleEntity] = []
            
            for key in schedule.keys {
                if let item: [String: Any?] = schedule[key] as? [String: Any?] {
                    guard let data = try? JSONSerialization.data(withJSONObject: item) else {
                        completion(nil)
                        return
                    }
                    let schedule: ScheduleEntity = try! JSONDecoder().decode(ScheduleEntity.self, from: data)
                    schedule.key = key
                    items.append(schedule)
                }
            }
            completion(items)
        }
    }
    
    static func scheduleTimeSet(completion: @escaping (([ScheduleTimeSetEntity]) -> Void)) {
        let dbRef: DatabaseReference = FirebaseAPI.reference.ref.child("scheduleTimeSet")
        
        dbRef.observeSingleEvent(of: .value) { (snapshot) in
            guard let scheduleTimeSet: [String: Any?] = snapshot.value as? [String: Any?] else {
                return
            }
            
            var items: [ScheduleTimeSetEntity] = []
            
            for key in scheduleTimeSet.keys {
                if let item: [String: Any?] = scheduleTimeSet[key] as? [String: Any?] {
                    guard let data = try? JSONSerialization.data(withJSONObject: item) else {
                        return
                    }
                    let timeSet: ScheduleTimeSetEntity = try! JSONDecoder().decode(ScheduleTimeSetEntity.self, from: data)
                    items.append(timeSet)
                }
            }
            self.timeArray = items
            completion(items)
        }
    }
    
    static func userData(completion: @escaping ((UserEntity?) -> Void)) {
        let dbRef: DatabaseReference = FirebaseAPI.referenceWithUserID.ref.child("userData")
        
        dbRef.observeSingleEvent(of: .value) { (snapshot) in
            guard let userData: [String: Any?] = snapshot.value as? [String: Any?] else {
                return
            }
            
            let user: UserEntity = UserEntity(firstName: userData["firstName"] as! String, secondName: userData["secondName"] as! String)
            
            self.currentUser = user
            completion(user)
        }
    }

    
    static func allPairs(completion: @escaping (([ScheduleEntity]?) -> Void)) {
        let dbRef: DatabaseReference = FirebaseAPI.referenceWithUserID.ref.child("schedule")
        
        var items: [ScheduleEntity] = []
        dbRef.observeSingleEvent(of: .value) { (snapshot) in
            guard let schedule: [String: Any?] = snapshot.value as? [String: Any?] else {
                ScheduleDatabase.scheduleArray = [ScheduleEntity]()
                completion(nil)
                return
            }         
            
            for key in schedule.keys {
                if let item: [String: Any?] = schedule[key] as? [String: Any?]{
                    guard let data = try? JSONSerialization.data(withJSONObject: item) else {
                        ScheduleDatabase.scheduleArray = [ScheduleEntity]()
                        completion(nil)
                        return
                    }
                    let schedule: ScheduleEntity = try! JSONDecoder().decode(ScheduleEntity.self, from: data)
                    schedule.key = key
                    items.append(schedule)
                }
            }
            self.scheduleArray = items.sorted()
            completion(items)
        }
    }
    
    static func typePair(completion: @escaping (([PairTypeEntity]) -> Void)) {
        let dbRef: DatabaseReference = FirebaseAPI.reference.ref.child("typePair")
        
        dbRef.observeSingleEvent(of: .value) { (snapshot) in
            guard let typePair: [String: Any?] = snapshot.value as? [String: Any?] else {
                return
            }
           
            var items: [PairTypeEntity] = []
            
            for key in typePair.keys {
                if let item: [String: Any] = typePair[key] as? [String: Any] {
                    guard let data = try? JSONSerialization.data(withJSONObject: item) else {
                        return
                    }
                    let pairType: PairTypeEntity = try! JSONDecoder().decode(PairTypeEntity.self, from: data)
                    items.append(pairType)                  
                }
            }
            self.typePairArray = items.sorted()
            completion(items)
        }
    }
    
    static func groups(completion: @escaping (([GroupEntity]?) -> Void)) {
        let dbRef: DatabaseReference = FirebaseAPI.referenceWithUserID.ref.child("groups")
        
        dbRef.observeSingleEvent(of: .value) { (snapshot) in
            guard let groups: [String: Any?] = snapshot.value as? [String: Any?] else {
                ScheduleDatabase.groupsArray = [GroupEntity]()
                completion(nil)
                return
            }
            
            var items: [GroupEntity] = []
            
            for key in groups.keys {
                
                if var itemGroup: [String: Any] = groups[key] as? [String: Any] {
                    if let itemStudent: [String: Any] = itemGroup["students"] as? [String: Any] {
                        items.append(GroupEntity(withStudent: itemGroup, key: key, studentSnapshot: itemStudent))
                    } else {
                        items.append(GroupEntity(withoutStudent: itemGroup, key: key))
                    }                    
                }
            }
            self.groupsArray = items
            completion(items)
        }
    }
    
    static func groupTypes(completion: @escaping (([GroupTypeEntity]) -> Void)) {
        let dbRef: DatabaseReference = FirebaseAPI.reference.ref.child("typeGroup")
        
        dbRef.observeSingleEvent(of: .value) { (snapshot) in
            guard let typeGroup: [String: Any?] = snapshot.value as? [String: Any?] else {
                return
            }
            
            var items: [GroupTypeEntity] = []
            
            for key in typeGroup.keys {
                if let item: [String: Any] = typeGroup[key] as? [String: Any] {
                    guard let data = try? JSONSerialization.data(withJSONObject: item) else {
                        return
                    }
                    let groupType: GroupTypeEntity = try! JSONDecoder().decode(GroupTypeEntity.self, from: data)
                    items.append(groupType)                   
                }
            }
            self.typeGroupArray = items.sorted()
            completion(items)
        }
    }
    
    static func group(byKey key: String, completion: @escaping ((GroupEntity) -> Void)) {
        let dbRef: DatabaseReference = FirebaseAPI.referenceWithUserID.ref.child("groups")
        
        dbRef.observeSingleEvent(of: .value) { (snapshot) in
            guard let group: [String: Any?] = snapshot.value as? [String: Any?] else {
                return
            }
            
            var items: GroupEntity!            
        
                if var itemGroup: [String: Any] = group[key] as? [String: Any] {
                    if let itemStudent: [String: Any] = itemGroup["students"] as? [String: Any] {
                        items = GroupEntity(withStudent: itemGroup, key: key, studentSnapshot: itemStudent)
                    } else {
                        items = GroupEntity(withoutStudent: itemGroup, key: key)
                    }
                }
            completion(items)
        }
    }
    
    static func attendance(completion: @escaping (([AttendanceEntity]?) -> Void)) {
        let dbRef: DatabaseReference = FirebaseAPI.referenceWithUserID.ref.child("attendance")
        
        dbRef.observeSingleEvent(of: .value) { (snapshot) in
            guard let attendance: [String: Any?] = snapshot.value as? [String: Any?] else {
                self.attendanceArray = [AttendanceEntity]()
                completion(nil)
                return
            }
            
            var items: [AttendanceEntity] = []
            
            for key in attendance.keys {
                if let item: [String: Any] = attendance[key] as? [String: Any] {
                    if let students: [String: Any] = item["students"] as? [String: Any] {
                        items.append(AttendanceEntity(key: key, studentSnapshot: students))
                    }
                }
            }
            
            self.attendanceArray = items
            completion(items)
        }
    }
    
    static func allData(completion: @escaping (() -> Void)) {
        let semaphore: DispatchSemaphore = DispatchSemaphore(value: 0)

        // TODO: refactor

        DispatchQueue.global().async {
            
            ScheduleDatabase.attendance(completion: { (data) in
                if data == nil {
                    ScheduleDatabase.attendanceArray = [AttendanceEntity]()
                    semaphore.signal()
                    return
                }
                semaphore.signal()
            })
            
            ScheduleDatabase.userData(completion: { (data) in
                semaphore.signal()
            })
            
            ScheduleDatabase.groups { (data) in
                semaphore.signal()
            }
         
            ScheduleDatabase.scheduleTimeSet { (data) in
                semaphore.signal()
            }
            
            ScheduleDatabase.typePair { (data) in
                semaphore.signal()
            }
            
            ScheduleDatabase.groupTypes{ (data) in
                semaphore.signal()
            }
            
            ScheduleDatabase.allPairs { (data) in
                if data == nil {
                    
                    semaphore.signal()
                    return
                }                
                semaphore.signal()               
            }
            
            for _ in 0...6 {
                semaphore.wait()
            }
            
            DispatchQueue.main.async {
                completion()
            }
        }
    }
    
    static func setEditStudent(_ student: StudentEntity, keyGroup: String, completion: @escaping (() -> Void)) {
        
        let pointsDB = FirebaseAPI.referenceWithUserID.ref.child("groups").child(keyGroup).child("students")
        
        let data = try? JSONEncoder().encode(student)
        
        guard let pointDictionary = try? ((JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any])!) else {
            return
        }
        
        pointsDB.child(student.key).setValue(pointDictionary) { (error, reference) in
            if error != nil {
                print(error!)
            } else {
                print("Message saved successfully!")
                
                ScheduleDatabase.groups { (data) in                   
                    completion()
                }
            }
        }
    }
    
    static func setSchedule(schedule: ScheduleEntity, byKey key: String, completion: @escaping (_ error: Error?) -> Void) {
        let pointsDB = FirebaseAPI.referenceWithUserID.ref.child("schedule").child(key)
        
        let data = try? JSONEncoder().encode(schedule)
        
        guard let pointDictionary = try? ((JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any])!) else {
            completion(nil)
            return
        }
        
        pointsDB.setValue(pointDictionary) { (error, reference) in
            if error != nil {
                completion(error)
            } else {
                completion(nil)
                print("Update successfully!")
            }
        }
    }
    
    static func setEditGroup(group: GroupEntity, byKey key: String, completion: @escaping (_ error: Error?) -> Void) {
        let pointsDB = FirebaseAPI.referenceWithUserID.ref.child("groups").child(key)
        
        let pointDictionary: [String: Any] = ["color": group.color,
                                              "grade": group.grade,
                                              "nameGroup": group.nameGroup,
                                              "typeGroup": group.typeGroup,
                                              "year": group.year]
        
        pointsDB.updateChildValues(pointDictionary) { (error, reference) in
            if error != nil {
                completion(error)
            } else {
                completion(nil)
                print("Update successfully!")
            }
        }
    }
    
    static func deleteSchedule(byKey key: String, completion: @escaping (_ error: Error?) -> Void) {
        let dbRef: DatabaseReference = FirebaseAPI.referenceWithUserID.ref.child("schedule")
        
        dbRef.child(key).removeValue { error, _ in
            if error != nil {
                completion(error)
            } else {
                completion(nil)
                print("Delete successfully!")
            }
        }
    }
    
    static func deleteGroup(byKey key: String,completion: @escaping (_ error: Error?) -> Void) {
        let dbRef: DatabaseReference = FirebaseAPI.referenceWithUserID.ref.child("groups")
        
        dbRef.child(key).removeValue { error, _ in
            if error != nil {
                completion(error)
            } else {
                completion(nil)
                print("Delete successfully!")
            }
        }
    }
    
    static func deleteStudent(byStudentKey stKey: String, groupKey: String, completion: @escaping (_ error: Error?) -> Void) {
        let dbRef: DatabaseReference = FirebaseAPI.referenceWithUserID.ref.child("groups").child(groupKey).child("students")
        
        dbRef.child(stKey).removeValue { error, _ in
            if error != nil {
                completion(error)
            } else {
                completion(nil)
                print("Delete successfully!")
            }
        }
    }
    
    static func deleteAttendance(byScheduleKey key: String, completion: @escaping (_ error: Error?) -> Void) {
        let dbRef: DatabaseReference = FirebaseAPI.referenceWithUserID.ref.child("attendance")
        
        dbRef.child(key).removeValue { error, _ in
            if error != nil {
                completion(error)
            } else {
                completion(nil)
                print("Delete successfully!")
            }
        }
    }
    
    static func localScheduleByDate(date: String) -> [ScheduleEntity] {
        var scheduleArrayNow: [ScheduleEntity] = [ScheduleEntity]()
        if ScheduleDatabase.scheduleArray.count > 0 {
            
            for i in 0...ScheduleDatabase.scheduleArray.count - 1 {
                
                if (Date.dateFromString(
                    dateStr: ScheduleDatabase.scheduleArray[i].date
                    ) == Date.dateFromString(dateStr: date)) {
                    scheduleArrayNow.append(ScheduleDatabase.scheduleArray[i])
                }
            }         
        }
        return scheduleArrayNow
    }
    
    static func takeIdTypeGroupFromTypeName(name: String) -> Int? {
        for type in self.typeGroupArray {
            if type.typeGroup == name {
                return type.id - 1
            }
        }
        return nil
    }
}
