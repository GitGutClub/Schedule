//
//  ScheduleEntity.swift
//  Schedule
//
//  Created by MacOS on 14.06.2019.
//  Copyright © 2019 GZ. All rights reserved.
//

import Foundation

class ScheduleEntity: Comparable, Codable {
    
    var nameDiscipline: String
    var nameGroup: String
    var numberHousing: Int
    var numberLectureHall: Int
    var numberPair: Int
    var typePair: String
    var date: String
    var active: Bool
    var key: String = ""   

    enum CodingKeys: String, CodingKey {        
        case nameDiscipline
        case nameGroup
        case numberHousing
        case numberLectureHall
        case numberPair
        case typePair
        case date
        case active        
    }
    
    init(numberHousing: Int, numberLectureHall: Int, numberPair: Int, nameGroup: String, nameDiscipline: String, typePair: String, date: String) {
        self.numberHousing = numberHousing
        self.numberLectureHall = numberLectureHall
        self.numberPair = numberPair
        self.nameGroup = nameGroup
        self.nameDiscipline = nameDiscipline
        self.typePair = typePair
        self.date = date
        self.active = true
    }    
    
    static func < (lhs: ScheduleEntity, rhs: ScheduleEntity) -> Bool {
        return lhs.numberPair < rhs.numberPair
    }
    
    static func == (lhs: ScheduleEntity, rhs: ScheduleEntity) -> Bool {
        return lhs.numberPair == rhs.numberPair
    }
    
}
