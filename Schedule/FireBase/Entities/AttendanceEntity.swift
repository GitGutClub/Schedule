//
//  File.swift
//  Schedule
//
//  Created by MacOS on 08.07.2019.
//  Copyright © 2019 GZ. All rights reserved.
//

import Foundation

class AttendanceEntity {
    
    private(set) var students: [StudentEntity] = []
    private(set) var scheduleKey: String = ""
    
    init(key: String, studentSnapshot: [String: Any]) {
        
    for key in studentSnapshot.keys {
        if let item: [String: Any] = studentSnapshot[key] as? [String: Any] {
            guard let data = try? JSONSerialization.data(withJSONObject: item) else {
                return
            }
            let student: StudentEntity = try! JSONDecoder().decode(StudentEntity.self, from: data)
            student.key = key
            self.students.append(student)
            
            }
        }
        self.students = self.students.sorted()
        self.scheduleKey = key
    }
}
