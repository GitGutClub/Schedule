//
//  GroupEntity.swift
//  Schedule
//
//  Created by MacOS on 14.06.2019.
//  Copyright © 2019 GZ. All rights reserved.
//

import Foundation

class GroupEntity: Codable {
    
    private(set) var nameGroup: String = ""
    private(set) var typeGroup: String = ""
    private(set) var students: [StudentEntity] = [StudentEntity]()
    private(set) var year: Int = 0
    private(set) var grade: Int = 0
    private(set) var key: String = ""
    private(set) var color: String = ""
    
    enum CodingKeys: String, CodingKey {
        case nameGroup
        case typeGroup
        case students
        case grade
        case color
        case year
    }
    
    init(withStudent snapshot: [String: Any], key: String, studentSnapshot: [String: Any]) {
        
        for key in studentSnapshot.keys {
            if let item: [String: Any] = studentSnapshot[key] as? [String: Any] {
                guard let data = try? JSONSerialization.data(withJSONObject: item) else {
                    return
                }
                let student: StudentEntity = try! JSONDecoder().decode(StudentEntity.self, from: data)
                student.key = key
                self.students.append(student)
            }
        }
        
        self.color = snapshot["color"] as! String
        self.students = self.students.sorted()
        self.typeGroup = snapshot["typeGroup"] as! String
        self.nameGroup = snapshot["nameGroup"] as! String
        self.year = snapshot["year"] as! Int
        self.grade = snapshot["grade"] as! Int
        self.key = key
    }
    
    init(withoutStudent snapshot: [String: Any], key: String) {
        self.students = [StudentEntity]()
        self.typeGroup = snapshot["typeGroup"] as! String
        self.nameGroup = snapshot["nameGroup"] as! String
        self.year = snapshot["year"] as! Int
        self.grade = snapshot["grade"] as! Int
        self.color = snapshot["color"] as! String
        self.key = key
    }
    
    init(name: String, type: String, year: Int, grade: Int, color: String) {
        self.typeGroup = type
        self.nameGroup = name
        self.year = year
        self.grade = grade
        self.color = color
    }   
}
