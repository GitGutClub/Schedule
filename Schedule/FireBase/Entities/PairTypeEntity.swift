//
//  TypeGroupEntity.swift
//  Schedule
//
//  Created by MacOS on 14.06.2019.
//  Copyright © 2019 GZ. All rights reserved.
//

import Foundation

class PairTypeEntity: Comparable, Codable {
    
    private(set) var typePair: String
    private(set) var id: Int   
    
    static func < (lhs: PairTypeEntity, rhs: PairTypeEntity) -> Bool {
        return lhs.id < rhs.id
    }
    
    static func == (lhs: PairTypeEntity, rhs: PairTypeEntity) -> Bool {
        return lhs.id == rhs.id
    }
}
