//
//  StudentEntity.swift
//  Schedule
//
//  Created by MacOS on 14.06.2019.
//  Copyright © 2019 GZ. All rights reserved.
//

import Foundation

class StudentEntity: Codable, Comparable {
    
    private(set) var firstName: String
    private(set) var secondName: String
    private(set) var profileImageURL: String
    private(set) var isHeadman: Bool
    private(set) var subgroup: Int
    private(set) var patronymicName: String?
    private(set) var phoneNumber: String?
    private(set) var email: String?
    private(set) var vk: String?
    private(set) var facebook: String?
    
    var studentDescription: String?
    var attendance: Bool?
    var key: String = ""
    
    enum CodingKeys: String, CodingKey {
        case firstName
        case secondName
        case profileImageURL
        case attendance
        case studentDescription
        case isHeadman
        case subgroup
        case patronymicName
        case phoneNumber
        case email
        case vk
        case facebook
    }
    
    init(firstName: String, secondName: String, profileImageURL: String, studentDescription: String?, isHeadman: Bool, subgroup: Int, patronymicName: String?, phoneNumber: String?, email: String?, vk: String?, facebook: String?) {       
        self.studentDescription = studentDescription
        self.patronymicName = patronymicName
        self.phoneNumber = phoneNumber
        self.email = email
        self.vk = vk
        self.facebook = facebook
        self.firstName = firstName
        self.secondName = secondName
        self.isHeadman = isHeadman
        self.subgroup = subgroup
        self.profileImageURL = profileImageURL
    }
    
    static func < (lhs: StudentEntity, rhs: StudentEntity) -> Bool {
       return lhs.firstName < rhs.firstName
    }
    
    static func == (lhs: StudentEntity, rhs: StudentEntity) -> Bool {
       return lhs.firstName == rhs.firstName
    }
}
