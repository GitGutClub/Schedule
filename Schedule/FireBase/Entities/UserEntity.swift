//
//  UserEntity.swift
//  Schedule
//
//  Created by MacOS on 21.07.2019.
//  Copyright © 2019 GZ. All rights reserved.
//

import Foundation

class UserEntity: Codable {
    
    private(set) var firstName: String = ""
    private(set) var secondName: String = ""
    
    enum CodingKeys: String, CodingKey {
        case firstName
        case secondName   
    }
    
    init() {
        
    }
    
    init(firstName: String, secondName: String) {
        self.firstName = firstName
        self.secondName = secondName
    }
}
