//
//  GroupTypeEntity.swift
//  Schedule
//
//  Created by MacOS on 23.06.2019.
//  Copyright © 2019 GZ. All rights reserved.
//

import Foundation

class GroupTypeEntity: Comparable, Codable {
    
    private(set) var typeGroup: String
    private(set) var id: Int  
    
    static func < (lhs: GroupTypeEntity, rhs: GroupTypeEntity) -> Bool {
        return lhs.id < rhs.id
    }
    
    static func == (lhs: GroupTypeEntity, rhs: GroupTypeEntity) -> Bool {
        return lhs.id == rhs.id
    }
}
