//
//  ScheduleTimeSetEntity.swift
//  Schedule
//
//  Created by MacOS on 14.06.2019.
//  Copyright © 2019 GZ. All rights reserved.
//

import Foundation

class ScheduleTimeSetEntity: Codable {    
    private(set) var beginTime: String
    private(set) var endTime: String
    private(set) var id: Int
}
