//
//  Authorization.swift
//  Schedule
//
//  Created by MacOS on 17.07.2019.
//  Copyright © 2019 GZ. All rights reserved.
//

import Foundation
import FirebaseAuth

class Authorization {
    
    static func createNewuser(withEmail email: String, password pass: String, firstName: String, secondName: String,  complition: @escaping (Error?) -> Void) {
        
        Auth.auth().createUser(withEmail: email, password: pass) { (userFB, error) in
            if error != nil {
                complition(error)
            } else {
                let user = UserEntity(firstName: firstName, secondName: secondName)
                SendData.sendUserData(user: user, uid: (userFB?.user.uid)!, completion: {
                    complition(nil)
                })                
            }
        }
    }
    
    static func signIn(withEmail email: String, password pass: String, complition: @escaping (Error?) -> Void) {
        
        Auth.auth().signIn(withEmail:  email, password: pass) { (user, error) in
            if error != nil {
                complition(error)
            } else {
                complition(nil)
            }
        }
    }
    
    static func isValid(nameString: String) -> (valid: Bool, errorText: String?)  {
        
        var returnValue = true
        let nameRegEx = "^[а-яА-ЯёЁ][а-яА-ЯёЁ]{1,30}$"
        var errorText: String?
        
        if nameString.count == 0 {
            returnValue = false
            errorText = "Слишком короткое Имя/Фамилия"
            return (returnValue, errorText)
        }
        
        if nameString.count > 30 {
            returnValue = false
            errorText = "Слишком длинное Имя/Фамилия"
            return (returnValue, errorText)
        }
        
        do {
            let regex = try NSRegularExpression(pattern: nameRegEx)
            let nsString = nameString as NSString
            let results = regex.matches(in: nameString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0 {
                errorText = "Только русские буквы"
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            errorText = "\(error)"
            returnValue = false
        }
        
        return  (returnValue, errorText)
    }
    
    static func isValid(emailAddressString: String) -> Bool {
        
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0 {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    
    static func isValid(passwordString: String) -> (valid: Bool, errorText: String?) {
        
        var returnValue = true
        var errorText: String?
        let passwordStringLowerCase = passwordString.lowercased()
        
        if passwordString.count < 6 {
            returnValue = false
            errorText = "Слишком короткий пароль"
            return (returnValue, errorText)
        }
        
        if passwordStringLowerCase == passwordString {
            returnValue = false
            errorText = "Как минимум 1 буква должна быть заглавной"
            return (returnValue, errorText)
        }
        
        do {
            
            let regex = try NSRegularExpression(pattern:  "(?=.*\\d)")
            let nsString = passwordString as NSString
            let results = regex.matches(in: passwordString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0 {
                errorText = "Как минимум 1 число"
                returnValue = false
                return (returnValue, errorText)
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            errorText = ""
            returnValue = false
        }
        
        do {
            let regex = try NSRegularExpression(pattern: "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{6,}$")
            let nsString = passwordString as NSString
            let results = regex.matches(in: passwordString, range: NSRange(location: 0, length: nsString.length))

            if results.count == 0 {
                errorText = "Только латиница"
                returnValue = false
            }

        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            errorText = "\(error)"
            returnValue = false
        }
        
        return  (returnValue, errorText)
    }
}
