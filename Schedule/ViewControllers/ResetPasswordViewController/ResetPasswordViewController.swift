//
//  ResetPasswordViewController.swift
//  Schedule
//
//  Created by MacOS on 22.07.2019.
//  Copyright © 2019 GZ. All rights reserved.
//

import UIKit
import FirebaseAuth
import NotificationCenter

class ResetPasswordViewController: BasicViewController {
    
    private var bottomConstraint: CGFloat!
    
    @IBOutlet var resetPassButton: UIButton!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var stackViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet var activity: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        bottomConstraint = stackViewBottomConstraint.constant
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        let frame = stackViewBottomConstraint.constant
        
        if frame == bottomConstraint {
            adjustingHeight(show: true, notification: notification)
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        let frame = stackViewBottomConstraint.constant
        
        if frame != bottomConstraint {
            adjustingHeight(show: false, notification: notification)
        }
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        DispatchQueue.main.async {
            GlobalRouter.shared.back()
        }
    }
    
    @IBAction func resetPassButtonPressed(_ sender: UIButton) {
        activity.startAnimating()
        
        if !Authorization.isValid(emailAddressString: emailTextField.text!) {
            activity.stopAnimating()
            UIAlertController.presentAlert(on: self, errorName: "Ошибка в почте")
            return
        }
        
        Auth.auth().sendPasswordReset(withEmail: emailTextField.text!) { error in
            self.activity.stopAnimating()
            
            UIAlertController.presentAlert(on: self, messegeText: "Письмо для восстановления отправленно", complition: {
                
                DispatchQueue.main.async {
                    GlobalRouter.shared.back()
                }
            })
        }      
    }
}


extension ResetPasswordViewController: UITextFieldDelegate {
    
    private func adjustingHeight(show:Bool, notification:NSNotification) {
        
        var userInfo = notification.userInfo!
        
        let keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        
        let animationDurarion = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as! TimeInterval
        
        let heighUp: CGFloat = 20
        
        let changeInHeight: CGFloat = (keyboardFrame.height + heighUp) * (show ? 1 : -1)
        
        UIView.animate(withDuration: animationDurarion) {
            if show {
                self.stackViewBottomConstraint.constant = changeInHeight
            } else {
                self.stackViewBottomConstraint.constant = self.bottomConstraint
            }
        }
    }
}
