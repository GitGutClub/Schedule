//
//  RegistrationViewController.swift
//  Schedule
//
//  Created by MacOS on 20.07.2019.
//  Copyright © 2019 GZ. All rights reserved.
//

import UIKit

class RegistrationViewController: BasicViewController {
    
    private var bottomConstraint: CGFloat!
    
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var firstNameTextField: UITextField!
    @IBOutlet var secondNameTextField: UITextField!
    @IBOutlet var passTextField: UITextField!
    @IBOutlet var secondPassTextField: UITextField!
    @IBOutlet var registrationButton: UIButton!
    @IBOutlet var infoTextView: UITextView!
    @IBOutlet var activity: UIActivityIndicatorView!
    @IBOutlet var dataStackViewBottomConstaint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        configureUI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    private func configureUI() {
        bottomConstraint = dataStackViewBottomConstaint.constant       
        configureTextFeilds()
        configureTextView()
    }
    
    private func configureTextFeilds() {
        emailTextField.delegate = self
        firstNameTextField.delegate = self
        secondNameTextField.delegate = self
        passTextField.delegate = self
        secondPassTextField.delegate = self
    }
    
    private func configureTextView() {
        let attributedString = NSMutableAttributedString(string: "By logging in, you agree to Schedule's Terms of Service Cookie, Privacy Policy and Content Policies")
        attributedString.addAttribute(.link, value: "https://www.hackingwithswift.com", range: NSRange(location: 28, length: 34))
        attributedString.addAttribute(.link, value: "https://www.hackingwithswift.com", range: NSRange(location: 64, length: 14))
        attributedString.addAttribute(.link, value: "https://www.hackingwithswift.com", range: NSRange(location: 83, length: 16))
        
        let linkAttributes: [NSAttributedString.Key : Any] = [
            
            NSAttributedString.Key.foregroundColor: UIColor.red,
            NSAttributedString.Key.underlineColor: UIColor.lightGray,
            NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        
        infoTextView.isUserInteractionEnabled = true
        infoTextView.isEditable = false
        infoTextView.linkTextAttributes = linkAttributes
        infoTextView.attributedText = attributedString
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        let frame = dataStackViewBottomConstaint.constant
        
        if frame == bottomConstraint {
            adjustingHeight(show: true, notification: notification)
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        let frame = dataStackViewBottomConstaint.constant
        
        if frame != bottomConstraint {
            adjustingHeight(show: false, notification: notification)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
            registrationButton.sendActions(for: .touchUpInside)
        }
        return false
    }    
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        UIApplication.shared.open(URL)
        return true
    }

    @IBAction func registrationButtonPressed(_ sender: UIButton) {       
        
        let firstNameValidate = Authorization.isValid(nameString: firstNameTextField.text!)
        let secondNameValidate = Authorization.isValid(nameString: secondNameTextField.text!)
        let passwordValidate = Authorization.isValid(passwordString: passTextField.text!)
        
        if !Authorization.isValid(emailAddressString: emailTextField.text!) {
            UIAlertController.presentAlert(on: self, errorName: "Ошибка в почте")
            return
        } else if !firstNameValidate.valid {
            UIAlertController.presentAlert(on: self, errorName: firstNameValidate.errorText!)
            return
        } else if !secondNameValidate.valid {
            UIAlertController.presentAlert(on: self, errorName: secondNameValidate.errorText!)
            return
        } else if !passwordValidate.valid {
            UIAlertController.presentAlert(on: self, errorName: passwordValidate.errorText!)
            return
        } else if passTextField.text! != secondPassTextField.text! {
            UIAlertController.presentAlert(on: self, errorName: "Пароли не совпадают")
            return
        }
        
        activity.startAnimating()
        
        Authorization.createNewuser(withEmail: emailTextField.text!, password: passTextField.text!, firstName: firstNameTextField.text!, secondName: secondNameTextField.text!) { (error) in
            if error != nil {
                self.activity.stopAnimating()
                UIAlertController.presentAlert(on: self, errorName: "Ошибка при регистрации \(error!)")
                print(error!)
            } else {
                Authorization.signIn(withEmail: self.emailTextField.text!, password: self.passTextField.text!) { (error) in
                    
                    if error != nil {
                        self.activity.stopAnimating()
                        UIAlertController.presentAlert(on: self, errorName: "Неверный логин, или пароль")
                        return
                    }
                    
                    ScheduleDatabase.allData {
                        self.activity.stopAnimating()
                        GlobalRouter.shared.showMainScreen()
                    }
                }
            }
        }
    }
    
    @IBAction func authButtonPressed(_ sender: Any) {
        DispatchQueue.main.async {
            GlobalRouter.shared.back()
        }
    }
}


extension RegistrationViewController: UITextFieldDelegate, UITextViewDelegate {
    private func adjustingHeight(show:Bool, notification:NSNotification) {
        
        var userInfo = notification.userInfo!
        
        let keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        
        let animationDurarion = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as! TimeInterval
        
        let heighUp: CGFloat = 20
        
        let changeInHeight: CGFloat = (keyboardFrame.height + heighUp) * (show ? 1 : -1) - keyboardFrame.height
        
        UIView.animate(withDuration: animationDurarion) {
            if show {
                self.dataStackViewBottomConstaint.constant += changeInHeight
            } else {
                self.dataStackViewBottomConstaint.constant = self.bottomConstraint
            }
        }
    } 
}
