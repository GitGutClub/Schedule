//
//  AuthorizationViewController.swift
//  Schedule
//
//  Created by MacOS on 17.07.2019.
//  Copyright © 2019 GZ. All rights reserved.
//

import UIKit
import FirebaseAuth
import NotificationCenter

class AuthorizationViewController: BasicViewController {
    
    private var bottomConstraint: CGFloat!
    
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var passTextField: UITextField!
    @IBOutlet var activity: UIActivityIndicatorView!
    @IBOutlet var signUpButton: UIButton!
    @IBOutlet var dataStackViewBottomConstaint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        configureUI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    private func configureUI() {
        bottomConstraint = dataStackViewBottomConstaint.constant       
        emailTextField.delegate = self
        passTextField.delegate = self
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        let frame = dataStackViewBottomConstaint.constant
        
        if frame == bottomConstraint {
            adjustingHeight(show: true, notification: notification)
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        let frame = dataStackViewBottomConstaint.constant
        
        if frame != bottomConstraint {
            adjustingHeight(show: false, notification: notification)
        }
    }

    @IBAction func signUpButtonPressed(_ sender: UIButton) {
        
        let passwordValidate = Authorization.isValid(passwordString: passTextField.text!)
        
        if !Authorization.isValid(emailAddressString: emailTextField.text!) {
            UIAlertController.presentAlert(on: self, errorName: "Ошибка в почте")
            return
        } else if !passwordValidate.valid {
            UIAlertController.presentAlert(on: self, errorName: passwordValidate.errorText!)
            return
        }
        
        self.activity.startAnimating()
        
        Authorization.signIn(withEmail: emailTextField.text!, password: passTextField.text!) { (error) in
            
            if error != nil {
                self.activity.stopAnimating()
                UIAlertController.presentAlert(on: self, errorName: "Неверный логин, или пароль")
                return
            }
            
            ScheduleDatabase.allData {
                self.activity.stopAnimating()
                GlobalRouter.shared.showMainScreen()
            }
        }
    }
   
    @IBAction func forgotPasswordButtonPressed(_ sender: UIButton) {
        DispatchQueue.main.async {
            GlobalRouter.shared.showResetPasswordScreen()
        }
    }
    
    @IBAction func registrationButtonPressed(_ sender: UIButton) {
        DispatchQueue.main.async {
            GlobalRouter.shared.showRegistrationScreen()
        }
    }
}


extension AuthorizationViewController: UITextFieldDelegate {    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
            signUpButton.sendActions(for: .touchUpInside)
        }
        return false
    }
    
    private func adjustingHeight(show:Bool, notification:NSNotification) {
        
        var userInfo = notification.userInfo!
        
        let keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        
        let animationDurarion = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as! TimeInterval
        
        let heighUp: CGFloat = 20
        
        let changeInHeight: CGFloat = (keyboardFrame.height + heighUp) * (show ? 1 : -1) - keyboardFrame.height
        
        UIView.animate(withDuration: animationDurarion) {
            if show {
                self.dataStackViewBottomConstaint.constant += changeInHeight
            } else {
                self.dataStackViewBottomConstaint.constant = self.bottomConstraint
            }
        }
    }   
}
