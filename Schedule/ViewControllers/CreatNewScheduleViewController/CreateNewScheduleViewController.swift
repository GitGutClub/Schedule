//
//  CreatNewScheduleViewController.swift
//  DataBase1
//
//  Created by MacOS on 02.05.2019.
//  Copyright © 2019 GZ. All rights reserved.
//

import UIKit
import Firebase
import PickerController
import Crashlytics
import NotificationCenter

class CreateNewScheduleViewController: BasicViewController {
    
    var router: ScheduleVCRouter!
    private var selectedTypePair: String?    
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var nameDisciplineTextField: TextField!
    @IBOutlet var nameGroupTextField: TextField!
    @IBOutlet var numberHousingTextField: TextField!
    @IBOutlet var numberLectureHallTextField: TextField!
    @IBOutlet var pairTypeTextField: TextField!    
    @IBOutlet var numberPairTextField: NumberPairTextField!
    @IBOutlet var dateTextField: TextField!
    @IBOutlet weak var stackView: UIStackView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Новая пара"
        router.navigationController.isNavigationBarHidden = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        configureUI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    private func configureUI() {        
        configureTextFields()
        configureBarItems()
    }
    
    private func configureBarItems() {
        
        let add = UIBarButtonItem(image: UIImage(named: "acceptIcon"),
                                  style: .done,
                                  target: self,
                                  action: #selector(createNewScheduleButtonPressed))
        
        navigationItem.rightBarButtonItems = [add]
    }
    
    private func configureTextFields() {
        numberHousingTextField.textField.keyboardType = UIKeyboardType.numberPad
        numberLectureHallTextField.textField.keyboardType = UIKeyboardType.numberPad
        numberPairTextField.textField.keyboardType = UIKeyboardType.numberPad
        
        let tapRecognizerGroup = UITapGestureRecognizer(target: self, action: #selector(showGroups))
        nameGroupTextField.isUserInteractionEnabled = true
        nameGroupTextField.textField.addGestureRecognizer(tapRecognizerGroup)
        
        let tapRecognizerPairType = UITapGestureRecognizer(target: self, action: #selector(showPairTypes))
        pairTypeTextField.isUserInteractionEnabled = true
        pairTypeTextField.textField.addGestureRecognizer(tapRecognizerPairType)
        
        let tapRecognizerDate = UITapGestureRecognizer(target: self, action: #selector(showPickerDate))
        dateTextField.isUserInteractionEnabled = true
        dateTextField.textField.addGestureRecognizer(tapRecognizerDate)
    }
    
    @objc private func keyboardWillShow(notification: NSNotification) {
        guard let keyboardFrame = notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        scrollView.contentInset.bottom = view.convert(keyboardFrame.cgRectValue, from: nil).size.height
    }
    
    @objc private func keyboardWillHide(notification: NSNotification) {
        scrollView.contentInset.bottom = 0
    }
    
    @objc func showPickerDate() {
        showDatePicker(title: "Выберите дату", onConfigure: { (datePicker) in
            datePicker.datePickerMode = UIDatePicker.Mode.date
            
        },
            onDone: { [weak self] (date) in
            self?.dateTextField.textField.text = Date.stringFromDate(date: date)
        },  onCancel: nil)
    }
    
    @objc func showPairTypes() {
        var data: [String] = []
        
        for item in ScheduleDatabase.typePairArray.sorted() {
            data.append(item.typePair)
        }
        
        showGroupPicker(title: "Выберите тип пары",
                        groupData: [data],
                        selectedItems: ["$0"],
                        
            onDone:
            { [weak self] (indices, items) in
                self?.selectedTypePair = items[0]
                self?.pairTypeTextField.textField.text = items[0]
                self?.view.endEditing(true)
            },
                        
            onCancel: nil)
    }
    
    @objc func showGroups(sender: UITapGestureRecognizer) {
        
        var data: [String] = []
        
        for item in ScheduleDatabase.groupsArray {
            data.append(item.nameGroup)
        }
        
        showGroupPicker(title: "Выберите группу",
                        groupData: [data],
                        selectedItems: ["$0"],
                        
            onDone:
            { [weak self] (indices, items) in
                self?.nameGroupTextField.textField.text = items[0]
                self?.view.endEditing(true)
            },
                        
            onCancel: {
                            
        })
    }
    
    @objc func createNewScheduleButtonPressed() {

        // TODO: вынести валидацию
        guard let nameDiscipline = nameDisciplineTextField.textField.text, nameDiscipline != "" else {
            nameDisciplineTextField.setValid(false)
            
            UIAlertController.presentAlert(on: self, errorName:"Ошибка в наименовании дисциплины.")
            return
        }
        
        guard let nameGroup = nameGroupTextField.textField.text, nameGroup != "" else {
            nameGroupTextField.setValid(false)
          
            UIAlertController.presentAlert(on: self, errorName:"Ошибка в названии группы.")
            return
        }
        
        guard let numberHousing = Int(numberHousingTextField.textField.text!) else {
            numberHousingTextField.setValid(false)
          
            UIAlertController.presentAlert(on: self, errorName:"Ошибка в номере корпуса.")
            return
        }
        
        guard let numberLectureHall = Int(numberLectureHallTextField.textField.text!) else {
            numberLectureHallTextField.setValid(false)
           
            UIAlertController.presentAlert(on: self, errorName:"Ошибка в номере аудитории.")
            return
        }
        
        guard let numberPair = Int(numberPairTextField.textField.text!), numberPair <= ScheduleDatabase.timeArray.count  else {
            numberPairTextField.setValid(false)
           
            UIAlertController.presentAlert(on: self, errorName:"Ошибка в номере пары.")
            return
        }
        
        guard let selectedTypePair = selectedTypePair else {
            pairTypeTextField.setValid(false)
           
            UIAlertController.presentAlert(on: self, errorName:"Не выбран тип пары.")
            return
        }
        
        guard let date = dateTextField.textField.text, date != "" else {
            dateTextField.setValid(false)
            
            UIAlertController.presentAlert(on: self, errorName:"Не выбрана дата.")
            return
        }
        
        let schedule: ScheduleEntity = ScheduleEntity(numberHousing: numberHousing,
                                                      numberLectureHall: numberLectureHall,
                                                      numberPair: numberPair,
                                                      nameGroup: nameGroup,
                                                      nameDiscipline: nameDiscipline,
                                                      typePair: selectedTypePair,
                                                      date: date)
        
        SendData.sendSchedule(schedule: schedule) {
            ScheduleDatabase.allPairs { (data) in              
                self.router.back()
            }
        }        
    }
}
