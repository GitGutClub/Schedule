//
//  GroupTableViewCell1.swift
//  Schedule
//
//  Created by MacOS on 01.06.2019.
//  Copyright © 2019 GZ. All rights reserved.
//

import UIKit


class GroupTableViewCell: UITableViewCell {
    
    @IBOutlet var nameGroupLabel: UILabel!
    @IBOutlet var typeGroupLabel: UILabel!
    @IBOutlet var studCountLabel: UILabel!
    @IBOutlet var yearLabel: UILabel!
    @IBOutlet var gradeLabel: UILabel!
    @IBOutlet var studIconLabel: UILabel!
    
    var group: GroupEntity?
       
    func setup(group: GroupEntity) {
    
        self.group = group
        self.nameGroupLabel.text = group.nameGroup
        self.typeGroupLabel.text = group.typeGroup
        self.yearLabel.text = String(group.year)
        self.gradeLabel.text = "\(group.grade) курс"
        self.studCountLabel.text = String(group.students.count)
        self.studIconLabel.text = String(group.nameGroup.first ?? "A").uppercased()
        self.studIconLabel.backgroundColor = UIColor(hex: group.color)
    }    
}
