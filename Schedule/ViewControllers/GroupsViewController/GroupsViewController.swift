//
//  ViewController.swift
//  Schedule
//
//  Created by MacOS on 09.05.2019.
//  Copyright © 2019 GZ. All rights reserved.
//

import UIKit

class GroupsViewController: UIViewController {
    
    var router: GroupsVCRouter!
    private let cellID = "GroupTableViewCell"
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var addNewGroupButton: CustomButtonToAdd!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Группы"
       
        configureUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {        
        tableView.reloadData()
    }
  
    override func viewWillAppear(_ animated: Bool) {
        router.navigationController.isNavigationBarHidden = true
        
    }
    
    private func configureUI() {        
        addNewGroupButton.delegate = self
        configureTableView()
    }
    
    private func configureTableView() {        
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableView.automaticDimension
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: cellID, bundle: nil),
                           forCellReuseIdentifier: cellID)
    }
}



extension GroupsViewController: UITableViewDataSource, UITableViewDelegate, CustomButtonDelegate {
    func buttonPressed() {
        router.showCreateNewGroupScreen()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ScheduleDatabase.groupsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID,
                                                 for: indexPath) as! GroupTableViewCell
        cell.setup(group: ScheduleDatabase.groupsArray[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        router.showStudentsScreen(group: ScheduleDatabase.groupsArray[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let deleteAction = UIContextualAction(style: .destructive, title: "") { (action, view, handler) in
            
            ScheduleDatabase.deleteGroup(byKey: ScheduleDatabase.groupsArray[indexPath.row].key, completion: { (error) in
                guard error == nil  else {
                    print("\(String(describing: error))")
                    return
                }
                ScheduleDatabase.groups(completion: { (data) in
                    self.tableView.reloadData()
                })
            })
        }
        
        let edit = UIContextualAction(style: .normal, title: "") { (action, view, handler) in
            self.router.showEditGroupScreen(withGroup: ScheduleDatabase.groupsArray[indexPath.row])
        }
        
        if let cgImageDelete = UIImage(named: "deleteIcon")?.cgImage {
            deleteAction.image = ImageWithoutRender(cgImage: cgImageDelete, scale: UIScreen.main.nativeScale, orientation: .up).sd_tintedImage(with: .white)
        }
      
        if let cgImageEdit = UIImage(named: "editIcon")?.cgImage {
            edit.image = ImageWithoutRender(cgImage: cgImageEdit, scale: UIScreen.main.nativeScale, orientation: .up)
        }
        
        edit.backgroundColor = UIColor.swipeActionBackgroungColor()
        deleteAction.backgroundColor = UIColor.red
        let configuration = UISwipeActionsConfiguration(actions: [deleteAction, edit])
        configuration.performsFirstActionWithFullSwipe = false
        
        return configuration
    }
}
