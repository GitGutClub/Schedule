//
//  ViewController.swift
//  DataBase1
//
//  Created by MacOS on 30.04.2019.
//  Copyright © 2019 GZ. All rights reserved.
//

import UIKit
import FirebaseAuth

class StartViewController: UIViewController {
    
    private let activityIndicator: UIActivityIndicatorView = {
        
        let activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
        activityIndicator.backgroundColor = UIColor(white: 0, alpha: 0.4)
        activityIndicator.hidesWhenStopped = true
        
        return activityIndicator
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        configureUI()
        nextScreen()
    }
    
    private func configureUI() {
        view.backgroundColor = UIColor.white
        view.addSubview(activityIndicator)
        activityIndicator.frame = view.bounds       
    }
    
    private func nextScreen() {
        // Проверяю соединение с интернетом, в случае успеха  загружаю данные и показываю основной экран, иначе экран отсутствия интернета
        if Reachability.isConnectedToNetwork() {
            checkIfUserIsSignedIn()
        } else {
            DispatchQueue.main.async { [weak self] in
                self?.activityIndicator.stopAnimating()
                GlobalRouter.shared.showInternetConnectionFailScreen()
            }
        }
    }
    
    private func checkIfUserIsSignedIn() {
        if Auth.auth().currentUser != nil {
            activityIndicator.startAnimating()
            ScheduleDatabase.allData { [weak self] in
                print("CONN")
                self?.activityIndicator.stopAnimating()
                GlobalRouter.shared.showMainScreen()                
            }
        } else {
            DispatchQueue.main.async {
                GlobalRouter.shared.showAuthorizationScreen()
            }
        }
    }
}
