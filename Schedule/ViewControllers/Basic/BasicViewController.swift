//
//  BasicTableViewController.swift
//  Schedule
//
//  Created by MacOS on 01.06.2019.
//  Copyright © 2019 GZ. All rights reserved.
//

import Foundation
import UIKit

class BasicViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        self.hideKeyboardWhenSwipeDown()
    }
}
