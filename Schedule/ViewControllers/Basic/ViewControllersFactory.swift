//
//  ViewControllersFactory.swift
//  Schedule
//
//  Created by MacOS on 15.05.2019.
//  Copyright © 2019 GZ. All rights reserved.
//

import Foundation

class ViewControllersFactory {
    
    static func takeScheduleViewController() -> ScheduleViewController {
        return ScheduleViewController(nibName: "\(ScheduleViewController.self)", bundle: nil)
    }
    
    static func takeMainTabBarController() -> MainTabBarController {
        return MainTabBarController(nibName: nil, bundle: nil)
    }
    
    static func takeGroupsViewController() -> GroupsViewController {
        return GroupsViewController(nibName: "\(GroupsViewController.self)", bundle: nil)
    }
    
    static func takeEditScheduleViewController() -> EditScheduleViewController {
        return EditScheduleViewController(nibName: "\(EditScheduleViewController.self)", bundle: nil)
    }
    
    static func takeCreateNewScheduleViewController() -> CreateNewScheduleViewController {
        return CreateNewScheduleViewController(nibName: "\(CreateNewScheduleViewController.self)", bundle: nil)
    }    
    
    static func takeStudentsViewController() -> StudentsViewController {
         return StudentsViewController(nibName: "\(StudentsViewController.self)", bundle: nil)
    }
    
    static func takeInternetConnectionFailViewController() -> InternetConnectionFailViewController {
        return  InternetConnectionFailViewController(nibName: "\(InternetConnectionFailViewController.self)", bundle: nil)
    }
    
    static func takeAttendanceViewController() -> AttendanceViewController {
        return AttendanceViewController(nibName: "\(AttendanceViewController.self)", bundle: nil)
    }
    
    static func takeAuthorizationViewController() -> AuthorizationViewController {
        return AuthorizationViewController(nibName: "\(AuthorizationViewController.self)", bundle: nil)
    }
    
    static func takeRegistrationViewController() -> RegistrationViewController {
        return RegistrationViewController(nibName: "\(RegistrationViewController.self)", bundle: nil)
    }
    
    static func takeResetPasswordViewController() -> ResetPasswordViewController {
        return ResetPasswordViewController(nibName: "\(ResetPasswordViewController.self)", bundle: nil)
    }
    
    static func takeConfigureStudentViewController() -> ConfigureStudentViewController {
        return ConfigureStudentViewController(nibName: "\(ConfigureStudentViewController.self)", bundle: nil)
    }
    
    static func takeConfigureGroupViewController() -> ConfigureGroupViewController {
        return ConfigureGroupViewController(nibName: "\(ConfigureGroupViewController.self)", bundle: nil)
    }
}
