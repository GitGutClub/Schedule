//
//  MainViewController.swift
//  Schedule
//
//  Created by MacOS on 08.05.2019.
//  Copyright © 2019 GZ. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureTabBar()
    }
    
    private func configureTabBar() {
      
        let routers = [ScheduleVCRouter().navigationController, GroupsVCRouter().navigationController, LogOutVCRouter().navigationController]
        
        viewControllers = routers.map {
            $0
        }
    }
}
