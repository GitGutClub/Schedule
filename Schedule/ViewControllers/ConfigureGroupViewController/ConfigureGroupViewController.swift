//
//  ConfigureGroupViewController.swift
//  Schedule
//
//  Created by MacOS on 07.08.2019.
//  Copyright © 2019 GZ. All rights reserved.
//

import UIKit
import NotificationCenter

class ConfigureGroupViewController: BasicViewController {
    
    var router: GroupsVCRouter!
    private var selectedType: String = ScheduleDatabase.typeGroupArray[0].typeGroup
    var group: GroupEntity?
    var viewHeight: CGFloat!
    var colors = GroupColors()
    var selectedColor: Color!
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var groupIconLabel: UILabel!
    @IBOutlet var typeGroupSegmentControl: UISegmentedControl! 
    @IBOutlet var nameGroupTextField: TextField!
    @IBOutlet var gradeTextField: TextField!
    @IBOutlet var yearTextField: TextField!  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeight = self.view.frame.origin.y
        if group != nil {
            title = "Редактирование группы"
        } else {
            title = "Новая группа"
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        configureUI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    private func configureUI() {
        selectedColor = colors.colorsArray[0]
        if let group = group {
            self.groupIconLabel.backgroundColor = UIColor(hex: group.color)
        }
        configureBarItems()
        configureTextFields()
        for i in 0...ScheduleDatabase.typeGroupArray.count - 1 {
            typeGroupSegmentControl.setTitle(ScheduleDatabase.typeGroupArray[i].typeGroup, forSegmentAt: i)
        }
    }
    
    private func configureBarItems() {
        router.navigationController.isNavigationBarHidden = false
        let add = UIBarButtonItem(image: UIImage(named: "acceptIcon"),
                                  style: .done,
                                  target: self,
                                  action: #selector(sendGroupButtonPressed))
        
        navigationItem.rightBarButtonItems = [add]
    }
    
    private func configureTextFields() {
        if let group = group {
            nameGroupTextField.textField.text = group.nameGroup
            gradeTextField.textField.text = String(group.grade)
            yearTextField.textField.text = String(group.year)
            groupIconLabel.backgroundColor = UIColor(hex: group.color)
            selectedColor = (hex: group.color, color: groupIconLabel.backgroundColor!)
            groupIconLabel.text = String(group.nameGroup.first ?? "A").uppercased()
            typeGroupSegmentControl.selectedSegmentIndex = ScheduleDatabase.takeIdTypeGroupFromTypeName(name: group.typeGroup) ?? 0
        } else {
            groupIconLabel.backgroundColor = selectedColor.color
        }
        nameGroupTextField.textField.delegate = self
        gradeTextField.textField.keyboardType = UIKeyboardType.numberPad
        let yearTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(showYearsPicker))
        yearTextField.textField.isUserInteractionEnabled = true
        yearTextField.textField.addGestureRecognizer(yearTapRecognizer)
    }
    
    @objc private func keyboardWillShow(notification: NSNotification) {
        guard let keyboardFrame = notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        scrollView.contentInset.bottom = view.convert(keyboardFrame.cgRectValue, from: nil).size.height
    }
    
    @objc private func keyboardWillHide(notification: NSNotification) {
        scrollView.contentInset.bottom = 0
    }
    
    @objc func showYearsPicker(sender: UITapGestureRecognizer) {
        let data: [String] = getYears()
        
        showGroupPicker(title: "Год набора группы",
                        groupData: [data],
                        selectedItems: ["$0"],
                        
                        onDone: { [weak self] (indices, items) in
                            self?.yearTextField.textField.text = items[0]
                            self?.view.endEditing(true)
            },
                        onCancel: nil)
    }
    
    private func getYears() -> [String] {
        var formattedDate: String? = ""
        
        let format = DateFormatter()
        format.dateFormat = "yyyy"
        formattedDate = format.string(from: Date())
        
        var yearsTillNow: [String] {
            var years = [String]()
            for i in (Int(formattedDate!)!-70..<Int(formattedDate!)!+1).reversed() {
                years.append("\(i)")
            }
            return years
        }
        
        return yearsTillNow
    }
    
    @IBAction func changeSubgroupPressed(_ sender: UISegmentedControl) {
        self.selectedType = ScheduleDatabase.typeGroupArray[sender.selectedSegmentIndex].typeGroup
    }
    
    @IBAction func colorButtonPressed(_ sender: UIButton) {
        selectedColor = colors.colorsArray[sender.tag]
        groupIconLabel.backgroundColor =  selectedColor.color
    }
    
    @IBAction func allColorsButtonPressed(_ sender: UIButton) {
        UIAlertController.presentAlert(on: self, errorName: "Кнопка будет доступна в будующем")
    }
    @objc func sendGroupButtonPressed() {
        guard let nameGroup = nameGroupTextField.textField.text, nameGroup != "" else {
            UIAlertController.presentAlert(on: self, errorName: "Введите название групппы")
            return
        }
        
        guard let yearGroup = Int(yearTextField.textField.text!) else {
            UIAlertController.presentAlert(on: self, errorName: "Некоректный год")
            return
        }
        
        guard let gradeGroup = Int(gradeTextField.textField.text!) else {
            UIAlertController.presentAlert(on: self, errorName: "Некоректный курс")
            return
        }
        
        self.navigationItem.rightBarButtonItem?.isEnabled = false
        
        if group == nil {
            let group = GroupEntity(name: nameGroup, type: selectedType, year: yearGroup, grade: gradeGroup, color: selectedColor.hex)
            
            // TODO: крутилки
            SendData.sendGroup(group: group) {                
                ScheduleDatabase.groups { [weak self] (data) in
                    self?.router.back()
                }
            }
        } else {
             let group = GroupEntity(name: nameGroup, type: selectedType, year: yearGroup, grade: gradeGroup, color: selectedColor.hex)           
            ScheduleDatabase.setEditGroup(group: group, byKey: (self.group?.key)!) { (error) in
                ScheduleDatabase.groups { [weak self] (data) in
                    self?.router.back()
                }
            }
        }
    }
}



extension ConfigureGroupViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        self.groupIconLabel.text = String(nameGroupTextField.textField.text?.first ?? "A").uppercased()
    }
}
