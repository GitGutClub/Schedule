//
//  ViewController.swift
//  DataBase1
//
//  Created by MacOS on 02.05.2019.
//  Copyright © 2019 GZ. All rights reserved.
//

import UIKit
import FirebaseAuth

class ScheduleViewController: UIViewController {
    
    private var dateNow: String = ""
    private var scheduleArrayNow: [ScheduleEntity] = [ScheduleEntity]()
    private let cellID = "ScheduleTableViewCell"
    private let collectionCellID = "ScheduleDateCollectionViewCell"
    private let collectionCellChangeMonthID = "ChangeMonthCollectionViewCell"
    private var dateAraray = [DateCellType]()
    private var dateOnTable: String = ""
    var router: ScheduleVCRouter!
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var monthNowLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Расписание"
        configureUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //Mark: Фикс бага с неправильным выделение дня при смене месяца и перехода на другой экран
        selectDayIfOnTable(withScroll: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {       
        router.navigationController.isNavigationBarHidden = true
        if Date.nowDateToString() == dateNow || dateNow == dateOnTable {
            scheduleArrayNow = ScheduleDatabase.localScheduleByDate(date: dateNow)
            dateOnTable = dateNow
        }
        dateAraray = Date.daysFromDate(date: dateNow)
        monthNowLabel.text = Date.monthNameStringFromDate(date: Date.dateFromString(dateStr: dateNow)!)
        tableView.reloadData()
        collectionView.reloadData()        
    }
    
    private func configureUI() {
        configureCollectionView()
        configureLabels()
        configureTableView()      
    }
    
    private func configureLabels() {
        dateNow = Date.nowDateToString()
        
        scheduleArrayNow = ScheduleDatabase.localScheduleByDate(date: dateNow)
    }
    
    private func configureCollectionView() {
        collectionView.register(UINib(nibName: collectionCellChangeMonthID, bundle: nil),
                                      forCellWithReuseIdentifier: collectionCellChangeMonthID)
        collectionView.register(UINib(nibName: collectionCellID, bundle: nil),
                                forCellWithReuseIdentifier: collectionCellID)
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    private func configureTableView() {
        tableView.register(UINib(nibName: cellID, bundle: nil),
                           forCellReuseIdentifier: cellID)
        tableView.allowsSelection = true
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableView.automaticDimension
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    private func updateUI(byDate date: Date, changeDaysDate: Int, changeMonth: Int) {
        let chandeMonthDate: Date = Date.changeMounth(date: date, by: changeMonth)
        let chandeDayDate: Date = Date.changeDays(date: chandeMonthDate, by: changeDaysDate)
        let dateForm: String = Date.stringFromDate(date: chandeDayDate)
        
        dateNow = dateForm
        dateAraray = Date.daysFromDate(date: dateForm)
        if changeMonth == 0 {
            dateOnTable = dateForm
            scheduleArrayNow = ScheduleDatabase.localScheduleByDate(date: dateForm)
            tableView.reloadData()
        }        
    }
    
    private func selectDayIfOnTable(withScroll: Bool) {
        if Date.dateFromString(dateStr: dateOnTable)?.takeMonthNumber() == dateAraray[0].date.takeMonthNumber() {
            collectionView.selectItem(at: IndexPath(item: (Date.dateFromString(dateStr: dateOnTable)?.takeDayNumber())!, section: 0), animated: true, scrollPosition: .init(rawValue: 0))
            if withScroll {
            collectionView.scrollToItem(at: IndexPath(item: (Date.dateFromString(dateStr: dateOnTable)?.takeDayNumber())!, section: 0), at: .centeredHorizontally, animated: true)
            }
        }
    }
    
    @IBAction func createNewPairButtonPressed(_ sender: UIButton) {
        router.showCreateNewScheduleScreen()
    }
}




extension ScheduleViewController: UITableViewDataSource, UITableViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, ChangeMonthCollectionViewCellDelegate {
    
    func tapOnLeftCell() {
        let collectionCellSize = 49
        updateUI(byDate: Date.dateFromString(dateStr: dateNow)! , changeDaysDate: 0, changeMonth: -1)
        monthNowLabel.text = Date.monthNameStringFromDate(date: Date.dateFromString(dateStr: dateNow)!)
        collectionView.reloadData()
        collectionView.setContentOffset(CGPoint(x: (dateAraray.count + 2) * collectionCellSize, y: 0), animated: true)
        selectDayIfOnTable(withScroll: false)
    }
    
    func tapOnRightCell() {
        updateUI(byDate: Date.dateFromString(dateStr: dateNow)! , changeDaysDate: 0, changeMonth: +1)
        monthNowLabel.text = Date.monthNameStringFromDate(date: Date.dateFromString(dateStr: dateNow)!)
        collectionView.reloadData()
        collectionView.setContentOffset(CGPoint(x:0,y:0), animated: true)
        selectDayIfOnTable(withScroll: false)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dateAraray.count + 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: collectionCellID,
                                                      for: indexPath) as! ScheduleDateCollectionViewCell
        if indexPath.row == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: collectionCellChangeMonthID,
                                                          for: indexPath) as! ChangeMonthCollectionViewCell
            cell.leftSite()
            cell.delegate = self
            
            return cell
            
        } else  if indexPath.row == dateAraray.count + 1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: collectionCellChangeMonthID,
                                                          for: indexPath) as! ChangeMonthCollectionViewCell
            cell.rightSite()
            cell.delegate = self
            
            return cell
            
        } else {
            cell.setup(dateCell: dateAraray[indexPath.row - 1])
        }
        
        return cell
    }    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row != 0 && indexPath.row != dateAraray.count + 1 {            
            updateUI(byDate: dateAraray[indexPath.row - 1].date, changeDaysDate: 0, changeMonth: 0)
        }
    }   
  
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // после нажатия на пару в рассписании, я провряю есть ли на это рассписание посещяемость
        for attendance in ScheduleDatabase.attendanceArray {
            if attendance.scheduleKey == scheduleArrayNow[indexPath.row].key { 
                router.showAttendanceScreen(nameGroup: scheduleArrayNow[indexPath.row].nameGroup, schedule: scheduleArrayNow[indexPath.row], isNew: false)
                return
            }
        }
        router.showAttendanceScreen(nameGroup: scheduleArrayNow[indexPath.row].nameGroup, schedule: scheduleArrayNow[indexPath.row], isNew: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int ) -> Int {
        return scheduleArrayNow.count
    }
    
    func tableView( _ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID,
                                                 for: indexPath) as! ScheduleTableViewCell
        
        cell.setupWith(schedule: scheduleArrayNow[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: "") { (action, view, handler) in
            
            ScheduleDatabase.deleteSchedule(byKey: self.scheduleArrayNow[indexPath.row].key) { [weak self] (error) in
                if let error = error {
                    print("\(String(describing: error))")
                    return
                }
                ScheduleDatabase.allPairs(completion: { (data) in                   
                    self?.scheduleArrayNow = ScheduleDatabase.localScheduleByDate(date: self?.dateNow ?? "")
                    self?.tableView.reloadData()
                })
            }
        }
        
        let edit = UIContextualAction(style: .normal, title: "") { (action, view, handler) in
            self.router.showEditScheduleScreen(schedule: self.scheduleArrayNow[indexPath.row])
        }
        
        if let cgImageDelete = UIImage(named: "deleteIcon")?.cgImage {
            deleteAction.image = ImageWithoutRender(cgImage: cgImageDelete, scale: UIScreen.main.nativeScale, orientation: .up).sd_tintedImage(with: .white)
        }
        
        if let cgImageEdit = UIImage(named: "editIcon")?.cgImage {
            edit.image = ImageWithoutRender(cgImage: cgImageEdit, scale: UIScreen.main.nativeScale, orientation: .up)
        }
        
        edit.backgroundColor = UIColor.swipeActionBackgroungColor()
        deleteAction.backgroundColor = UIColor.red
        
        let configuration = UISwipeActionsConfiguration(actions: [deleteAction, edit])
        configuration.performsFirstActionWithFullSwipe = false
        
        return configuration
    }
}
