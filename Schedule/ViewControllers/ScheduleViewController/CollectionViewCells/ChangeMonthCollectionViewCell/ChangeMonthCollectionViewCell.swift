//
//  ChangeMonthCollectionViewCell.swift
//  Schedule
//
//  Created by MacOS on 29.07.2019.
//  Copyright © 2019 GZ. All rights reserved.
//

import UIKit

protocol ChangeMonthCollectionViewCellDelegate {
    func tapOnLeftCell()
    func tapOnRightCell()
}

class ChangeMonthCollectionViewCell: UICollectionViewCell {
    
    var isLeft: Bool = true
    var delegate: ChangeMonthCollectionViewCellDelegate?
    
    @IBOutlet var siteLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(labelTapped))
        siteLabel.addGestureRecognizer(recognizer)
    }
    
    @objc private func labelTapped(){
        if isLeft{
            delegate?.tapOnLeftCell()
        } else {
            delegate?.tapOnRightCell()
        }
    }
    
    func leftSite() {
        self.siteLabel.text = "<"
        self.isLeft = true
    }
    
    func rightSite() {
        self.siteLabel.text = ">"
        self.isLeft = false
    }
}
