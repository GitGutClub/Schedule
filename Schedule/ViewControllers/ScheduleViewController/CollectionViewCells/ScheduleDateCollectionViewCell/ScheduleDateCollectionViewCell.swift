//
//  ScheduleDateCollectionViewCell.swift
//  Schedule
//
//  Created by MacOS on 26.07.2019.
//  Copyright © 2019 GZ. All rights reserved.
//

import UIKit

class ScheduleDateCollectionViewCell: UICollectionViewCell {

    var date: Date!
  
    override var isSelected: Bool {
        didSet {
            if self.isSelected {
                self.view.backgroundColor = UIColor.white
                self.dateLabel.textColor = UIColor.black
                self.dayLabel.textColor  = UIColor.black
            } else {
                self.view.backgroundColor =  UIColor.white.withAlphaComponent(0.25)
                self.dateLabel.textColor =  UIColor.white
                self.dayLabel.textColor  = UIColor.white
            }
        }
    }

    @IBOutlet var view: UIView!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var dayLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func setup(dateCell: DateCellType){
        self.date = dateCell.date
        self.dateLabel.text = dateCell.dateNumber
        self.dayLabel.text = dateCell.day
    }
}
