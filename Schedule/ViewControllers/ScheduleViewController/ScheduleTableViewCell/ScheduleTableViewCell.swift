//
//  TableViewCell.swift
//  DataBase1
//
//  Created by MacOS on 28.03.2019.
//  Copyright © 2019 GZ. All rights reserved.
//

import UIKit

class ScheduleTableViewCell: UITableViewCell {
    
    var key: String?
    var schedule: ScheduleEntity?
    
    @IBOutlet var numberHousingLabel: UILabel!
    @IBOutlet var numberPairLabel: UILabel!
    @IBOutlet weak var stackTimeView: UIStackView!
    @IBOutlet weak var stackWithInfo: UIStackView!
    @IBOutlet weak var nameDisciplineLabel: UILabel!
    @IBOutlet weak var nameGroupLabel: UILabel!
    @IBOutlet weak var timeBeginLabel: UILabel!
    @IBOutlet weak var timeEndLabel: UILabel!
    @IBOutlet weak var typePairLabel: UILabel!
    @IBOutlet weak var cellButton: UIButton!
    
    func setupWith(schedule: ScheduleEntity) {
        self.nameGroupLabel.text = schedule.nameGroup
        self.nameDisciplineLabel.text = schedule.nameDiscipline
        self.typePairLabel.text = schedule.typePair
        for id in ScheduleDatabase.timeArray {
            if id.id == schedule.numberPair {
                self.timeBeginLabel.text = id.beginTime
                self.timeEndLabel.text = id.endTime
            }
        }
        self.numberHousingLabel.text = "\(schedule.numberLectureHall)/\(schedule.numberHousing)"
        self.numberPairLabel.text = String(schedule.numberPair)
        self.schedule = schedule
        self.key = schedule.key
    }
}
