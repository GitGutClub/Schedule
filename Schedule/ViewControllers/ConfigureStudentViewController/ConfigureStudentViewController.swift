//
//  ConfigureStudentViewController.swift
//  Schedule
//
//  Created by MacOS on 05.08.2019.
//  Copyright © 2019 GZ. All rights reserved.
//

import UIKit
import NotificationCenter
import AVKit
import Photos
import SDWebImage

class ConfigureStudentViewController: BasicViewController {
   
    var groupKey: String!
    var student: StudentEntity?
    private var subgroup: Int = 1
    private var isHeadman: Bool = false
    var router: GroupsVCRouter!
    private var isChangeAvatar = false
    
    @IBOutlet var contentView: UIView!
    @IBOutlet var constraintContentHeight: NSLayoutConstraint!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var activity: UIActivityIndicatorView!
    @IBOutlet var firstNameTextField: TextField!
    @IBOutlet var secondNameTextField: TextField!
    @IBOutlet var discriptionTextField: UITextField!
    @IBOutlet var phoneNumberTextField: TextField!
    @IBOutlet var emailTextField: TextField!
    @IBOutlet var VkTextField: TextField!
    @IBOutlet var FacebookTextField: TextField!
    @IBOutlet var studentAvatarImageView: UIImageView!
    @IBOutlet var patronymicNameTextField: TextField!
    @IBOutlet var isHeadmanSwitch: UISwitch!
    @IBOutlet var subgroupSegmentControl: UISegmentedControl!
    @IBOutlet var deleteStudentButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if student != nil {
            title = "Редактирование студента"
        } else {
            title = "Новый студент"
        }

        configureUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    private func configureUI() {
        configureTextFields()
        if let student = student {
            studentAvatarImageView.sd_setImage(with: URL(string: student.profileImageURL))
        } else {
            deleteStudentButton.isHidden = true
        }
        configureSegmentControll()
        configureBarItems()
    }
    
    private func configureSegmentControll() {
        subgroupSegmentControl.backgroundColor = self.view.backgroundColor
        subgroupSegmentControl.layer.cornerRadius = subgroupSegmentControl.frame.height / 2.0
        for view in subgroupSegmentControl.subviews {           
            view.layer.borderColor = self.view.backgroundColor?.cgColor
            view.layer.borderWidth = CGFloat(1.2)
            view.layer.cornerRadius = CGFloat(subgroupSegmentControl.frame.height / 2.0)
            view.layer.masksToBounds = true
        }
    }
    
    private func configureTextFields() {
        firstNameTextField.textField.delegate = self
        secondNameTextField.textField.delegate = self
        phoneNumberTextField.textField.delegate = self
        emailTextField.textField.delegate = self
        VkTextField.textField.delegate = self
        FacebookTextField.textField.delegate = self
        discriptionTextField.delegate = self
        discriptionTextField.setBottomBorder()
        if let student = student {
            discriptionTextField.text = student.studentDescription
            firstNameTextField.textField.text = student.firstName
            secondNameTextField.textField.text = student.secondName
            patronymicNameTextField.textField.text = student.patronymicName
            emailTextField.textField.text = student.email
            phoneNumberTextField.textField.text = student.phoneNumber
            VkTextField.textField.text = student.vk
            FacebookTextField.textField.text = student.facebook
            isHeadmanSwitch.setOn(student.isHeadman, animated: true)
            self.isHeadman = student.isHeadman
            self.subgroup = student.subgroup
            subgroupSegmentControl.selectedSegmentIndex = student.subgroup - 1
        }
        
        emailTextField.textField.keyboardType = UIKeyboardType.emailAddress
        phoneNumberTextField.textField.keyboardType = UIKeyboardType.phonePad
    }
    
    private func configureBarItems() {        
       
        let add = UIBarButtonItem(image: UIImage(named: "acceptIcon"),
                                  style: .done,
                                  target: self,
                                  action: #selector(sendEditStudent))
        
        navigationItem.rightBarButtonItems = [add]
    }
    
    @objc private func keyboardWillShow(notification: NSNotification) {
        guard let keyboardFrame = notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        scrollView.contentInset.bottom = view.convert(keyboardFrame.cgRectValue, from: nil).size.height
    }
    
    @objc private func keyboardWillHide(notification: NSNotification) {
        scrollView.contentInset.bottom = 0
    }
    
    @objc private func sendEditStudent() {
        guard let firstName = firstNameTextField.textField.text, firstName != "" else {
            UIAlertController.presentAlert(on: self, errorName: "Введите имя")
            return
        }
        
        guard let secondName = secondNameTextField.textField.text, secondName != "" else {
            UIAlertController.presentAlert(on: self, errorName: "Введите фамилию")
            return
        }
        self.navigationItem.rightBarButtonItem?.isEnabled = false
        activity.startAnimating()
        if isChangeAvatar && student != nil {
            GroupFilebase.uploadStudentProfileImage(studentAvatarImageView.image!, name: NSUUID().uuidString) { [weak self] (url) in
                guard let stringUrl = url?.absoluteString else { return }
                
                let editStudent = StudentEntity(firstName: firstName,
                                                secondName: secondName,
                                                profileImageURL: stringUrl,
                                                studentDescription: self?.discriptionTextField.text,
                                                isHeadman: self?.isHeadman ?? false,
                                                subgroup: self?.subgroup ?? 1,
                                                patronymicName: self?.patronymicNameTextField.textField.text,
                                                phoneNumber: self?.phoneNumberTextField.textField.text,
                                                email: self?.emailTextField.textField.text,
                                                vk: self?.VkTextField.textField.text,
                                                facebook: self?.FacebookTextField.textField.text)
                
                editStudent.key = (self?.student?.key)!
                
                ScheduleDatabase.setEditStudent(editStudent, keyGroup: (self?.groupKey)!) { [weak self] in
                    self?.activity.stopAnimating()
                    self?.router.back()
                }
            }
        } else if student == nil {
            GroupFilebase.uploadStudentProfileImage(studentAvatarImageView.image!, name: NSUUID().uuidString) { [weak self] (url) in
                guard let stringUrl = url?.absoluteString else { return }
                
                let newStudent = StudentEntity(firstName: firstName,
                                                secondName: secondName,
                                                profileImageURL: stringUrl,
                                                studentDescription: self?.discriptionTextField.text,
                                                isHeadman: self?.isHeadman ?? false,
                                                subgroup: self?.subgroup ?? 1,
                                                patronymicName: self?.patronymicNameTextField.textField.text,
                                                phoneNumber: self?.phoneNumberTextField.textField.text,
                                                email: self?.emailTextField.textField.text,
                                                vk: self?.VkTextField.textField.text,
                                                facebook: self?.FacebookTextField.textField.text)
                
                SendData.sendNewStudent(newStudent, keyGroup: (self?.groupKey)!, completion: {
                    self?.activity.stopAnimating()
                    self?.router.back()
                })
            }
        } else if !isChangeAvatar && student != nil {
            let editStudent = StudentEntity(firstName: firstName,
                                            secondName: secondName,
                                            profileImageURL: (student?.profileImageURL)!,
                                            studentDescription: self.discriptionTextField.text,
                                            isHeadman: self.isHeadman,
                                            subgroup: self.subgroup,
                                            patronymicName: self.patronymicNameTextField.textField.text,
                                            phoneNumber: self.phoneNumberTextField.textField.text,
                                            email: self.emailTextField.textField.text,
                                            vk: self.VkTextField.textField.text,
                                            facebook: self.FacebookTextField.textField.text)
            
            editStudent.key = self.student!.key
            
            ScheduleDatabase.setEditStudent(editStudent, keyGroup: self.groupKey) { [weak self] in
                self?.activity.stopAnimating()
                self?.router.back()
            }
        }
    }
    
    @IBAction func changeSubgroupPressed(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            self.subgroup = 1
        } else if sender.selectedSegmentIndex == 1 {
            self.subgroup = 2
        }
    }
    
    @IBAction func deleteStudentButtonPressed(_ sender: UIButton) {
        UIAlertController.presentAlert(on: self, errorName: "Кнопка будет доступна в будующем")
    }
    
    @IBAction func changeAvatarPhotoButtonPressed(_ sender: UIButton) {
        let takePhoto = UIAlertAction(title: "Выбрать фото из галереи", style: .default) { (alert) in
            let photos = PHPhotoLibrary.authorizationStatus()
            if photos == .authorized {
                self.presentImagePicker(withType: .photoLibrary)
            }
            else if photos == .notDetermined {
                PHPhotoLibrary.requestAuthorization({status in
                    if status == .authorized {
                        self.presentImagePicker(withType: .photoLibrary)
                        return
                    } else {
                        return
                    }
                })
            }
        }
        
        let makePhoto = UIAlertAction(title: "Сделать фото", style: .default) { (alert) in
            AVCaptureDevice.requestAccess(for: AVMediaType.video) { response in
                if response {
                    self.presentImagePicker(withType: .camera)
                    return
                } else {
                    return
                }
            }
            self.presentImagePicker(withType: .camera)
        }
        UIAlertController.presentActionSheet(on: self, name: "Как получить фото?", buttons: [takePhoto, makePhoto])
    }
    
    @IBAction func changeHeadmanGroupPressed(_ sender: UISwitch) {
        self.isHeadman = sender.isOn
    }
}


extension ConfigureStudentViewController: UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func presentImagePicker(withType type: UIImagePickerController.SourceType) {
        let picker = UIImagePickerController()
        picker.sourceType = type
        picker.delegate = self
        picker.allowsEditing = true
        present(picker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        var selectedImage: UIImage?
        
        if let editingImage = info[.editedImage] as? UIImage {
            selectedImage = editingImage
            isChangeAvatar = true
        } else if let originalImage = info[.originalImage] as? UIImage {
            isChangeAvatar = true
            selectedImage = originalImage
        }
        
        let size = CGSize(width: 90.0, height: 90.0)
        self.studentAvatarImageView.image = selectedImage?.sd_resizedImage(with: size, scaleMode: .aspectFill)
        navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        navigationController?.dismiss(animated: true, completion: nil)
        print("cancel")
    }
}
