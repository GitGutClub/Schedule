//
//  AttendanceTableViewCell.swift
//  Schedule
//
//  Created by MacOS on 08.07.2019.
//  Copyright © 2019 GZ. All rights reserved.
//

import UIKit
// Протокол для Switch'a, фиксирующий изменение его положения 
protocol AttendanceTableViewCellDelegate {
    func setSwitch(value : Bool,  index: Int)
}

class AttendanceTableViewCell: UITableViewCell {
    
    var student: StudentEntity?
    var delegate: AttendanceTableViewCellDelegate?
    var index: Int!    
   
    @IBOutlet var studentAvatarImage: UIImageView!
    @IBOutlet var firstNameLabel: UILabel!
    @IBOutlet var secondNameLabel: UILabel!
    @IBOutlet var studentSwitch: CustomSwitch!
    @IBOutlet var studentDescriptionLabel: UILabel!
    @IBOutlet var subgroupLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()       
        studentSwitch.customSwitch.addTarget(self, action: #selector(set), for: UIControl.Event.valueChanged)
    }
    
    override func layoutSubviews() {
        studentAvatarImage.layer.cornerRadius =  10
        studentAvatarImage.layer.masksToBounds = true
        subgroupLabel.layer.borderWidth = 2
        subgroupLabel.layer.borderColor = UIColor.white.cgColor
        subgroupLabel.layer.cornerRadius = 2
        subgroupLabel.layer.masksToBounds = true
    }
    
    @objc func set() {
        studentSwitch.offLabel.isHidden = studentSwitch.customSwitch.isOn
        delegate?.setSwitch(value: studentSwitch.customSwitch.isOn, index: index)
    }
    
    func setup(with student: StudentEntity) {
        studentAvatarImage.image = UIImage(named: "group")
        subgroupLabel.text = String(student.subgroup)
        setHeadman(isHeadman: student.isHeadman)
        self.student = student
        self.firstNameLabel.text = student.firstName
        self.secondNameLabel.text = student.secondName
        self.studentDescriptionLabel.text = student.studentDescription
        self.studentSwitch.customSwitch.isOn = student.attendance ?? false
        let url = URL(string: student.profileImageURL)
        studentAvatarImage.sd_setImage(with: url)
    }
    
    private func setHeadman(isHeadman: Bool) {
        if isHeadman {
            subgroupLabel.backgroundColor = .red; subgroupLabel.textColor = .white
        } else {
            subgroupLabel.backgroundColor = .white; subgroupLabel.textColor = .black
        }
    }
}
