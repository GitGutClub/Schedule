//
//  AttendanceViewModel.swift
//  Schedule
//
//  Created by MacOS on 08.07.2019.
//  Copyright © 2019 GZ. All rights reserved.
//

import Foundation

class AttendanceViewModel {
    
    var students: [StudentEntity] = [StudentEntity]()
    
    func fetchByGroupkey(key: String, completion: @escaping () -> Void) {
        for item in ScheduleDatabase.groupsArray {
            if item.key == key {
                self.students = item.students
                break
            }
        }
        completion()
    }
    
    func fetchByGroupName(name: String, completion: @escaping (GroupEntity?) -> Void) {
        for item in ScheduleDatabase.groupsArray {
            if item.nameGroup == name {
                self.students = item.students
                completion(item)
                break
            }
        }
        completion(nil)
    }
    // этот метод ищет не в группах а в списках посещяемости, но только в том случае если на нужное рассписание она отмечена
    func fetchByScheduleKey(key: String, completion: @escaping () -> Void) {
        for item in ScheduleDatabase.attendanceArray {
            if item.scheduleKey == key {
                self.students = item.students
                break
            }
        }
        completion()
    }
}
