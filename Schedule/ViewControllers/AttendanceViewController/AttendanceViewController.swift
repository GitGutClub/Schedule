//
//  AttendanceViewController.swift
//  Schedule
//
//  Created by MacOS on 08.07.2019.
//  Copyright © 2019 GZ. All rights reserved.
//

import UIKit

class AttendanceViewController: UIViewController {
    
    private var studentViewModel: AttendanceViewModel = AttendanceViewModel()
    private let cellID = "AttendanceTableViewCell"
    private var studentsComplition: [StudentEntity]!
    private var group: GroupEntity?
    var nameGroup = ""
    var schedule: ScheduleEntity!   
    var isNew: Bool!
    var router: ScheduleVCRouter!    
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var activity: UIActivityIndicatorView!
    @IBOutlet var nameDisciplineLabel: UILabel!
    @IBOutlet var nameGroupLabel: UILabel!
    @IBOutlet var typeGroupLabel: UILabel!
    @IBOutlet var studentCountLabel: UILabel!
    @IBOutlet var yearGroupLabel: UILabel!
    @IBOutlet var gradeGroupLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        router.navigationController.isNavigationBarHidden = false
        configureUI()
    }    
    
    override func viewWillAppear(_ animated: Bool) {
        // Если посещаемость новая то студенты ищутся в группах, а если нет то в списках посещаемости
        configureTableViewData()
    }
    
    private func configureUI() {
        nameDisciplineLabel.text = schedule.nameDiscipline
        configureBarItems()
        configureTableView()
    }
    
    private func configureTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: cellID, bundle: nil),
                           forCellReuseIdentifier: cellID)
    }
    
    private func configureLabelsData() {
        if let group = group {
            self.nameGroupLabel.text = group.nameGroup
            self.yearGroupLabel.text = String(group.year)
            self.gradeGroupLabel.text = "\(group.grade) Курс"
            self.view.backgroundColor = UIColor(hex: group.color)
        }
        self.studentCountLabel.text = "\(studentViewModel.students.count)"
    }
    
    private func configureTableViewData() {
        switchIsNewFunction(trueFunc: {
            
            studentViewModel.fetchByGroupName(name: nameGroup) { [weak self] (group) in
                self?.group = group
                self?.configureLabelsData()
                self?.studentsComplition = self?.studentViewModel.students
                self?.tableView.reloadData()
            }
            // Присваиваю каждому студенту из текущего экрана дефолтное значение посещения если посещаемось новая
            if !self.studentsComplition.isEmpty {
                for i in 0...self.studentsComplition.count - 1 {
                    self.studentsComplition[i].attendance = false
                }
            }
        }, falseFunc: {
            studentViewModel.fetchByGroupName(name: self.schedule.nameGroup , completion: { [weak self] (group) in
                self?.studentViewModel.fetchByScheduleKey(key: self?.schedule.key ?? "") {
                    self?.group = group
                    self?.configureLabelsData()
                    self?.studentsComplition = self?.studentViewModel.students
                    self?.tableView.reloadData()
                }
            })
        })
    }
    
    private func configureBarItems() {
        // Если на текущую пару есть посещаемость, кнопка удалить, а если нет, то отметить новую посещаемсть
        switchIsNewFunction(trueFunc: {
            
            let add = UIBarButtonItem(
                 barButtonSystemItem: .done,
                 target: self,
                 action: #selector(attendanceComplete))
            
            navigationItem.rightBarButtonItems = [add]
            
        }, falseFunc: {
            let edit = UIBarButtonItem(image: UIImage(named: "editIcon"),
                                       style: .plain,
                                       target: self,
                                       action: #selector(editAttendance))
            
            let delete = UIBarButtonItem(image: UIImage(named: "deleteIcon"),
                                         style: .plain,
                                         target: self,
                                         action: #selector(attendanceDelete))
            
            navigationItem.rightBarButtonItems = [delete, edit]
        })
    }
    
    @objc private func editAttendance() {
        UIAlertController.presentAlert(on: self, errorName: "Кнопка редактирования будет доступна в будующем")
    }
    
    private func switchIsNewFunction(trueFunc: () -> (), falseFunc: () -> ()) {
        switch isNew {
        case true:
            trueFunc()
        case false:
            falseFunc()
        default:
            break
        }
    }
    
    @objc func attendanceComplete() {
        // Создание новой посещаемости
        activity.startAnimating()
        if studentsComplition.isEmpty {
            activity.stopAnimating()
            UIAlertController.presentAlert(on: self, errorName: "Нет студентов")
            return
        }
        SendData.sendAttendance(students: studentsComplition, scheduleKey: schedule.key) {
            ScheduleDatabase.attendance(completion: { [weak self] (data) in
                self?.activity.stopAnimating()
                self?.router.back()
            })
        }
    }
    
    @objc func attendanceDelete() {
        // Удаление посещаемости
        activity.startAnimating()
        ScheduleDatabase.deleteAttendance(byScheduleKey: schedule.key) { (error) in
            if error == nil {
                ScheduleDatabase.attendance(completion: { [weak self] (data) in                 
                    self?.activity.stopAnimating()
                    self?.router.back()
                })
            }
        }
    }
}



extension AttendanceViewController: UITableViewDelegate, UITableViewDataSource, AttendanceTableViewCellDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {    
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func setSwitch(value: Bool, index: Int) {
        // Метод делегата фикссирующий изменение положения Swith'a
        studentsComplition[index].attendance = value
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return studentViewModel.students.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID,
                                                 for: indexPath) as! AttendanceTableViewCell
        
        cell.setup(with: studentViewModel.students[indexPath.row])
        // Если посещаемость не новая, то блокирую Swith
        if !isNew {
            cell.studentSwitch.customSwitch.isEnabled = false
        }
        cell.index = indexPath.row
        cell.delegate = self
        
        return cell
    }
}
