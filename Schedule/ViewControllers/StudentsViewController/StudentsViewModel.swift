//
//  StudentsViewModel.swift
//  Schedule
//
//  Created by MacOS on 13.06.2019.
//  Copyright © 2019 GZ. All rights reserved.
//

import Foundation

class StudentsViewModel {
    
    var students: [StudentEntity] = [StudentEntity]()  
    
    func fetchByGroupkey(key: String, completion: @escaping () -> Void) {
        for item in ScheduleDatabase.groupsArray {            
            if item.key == key {
                self.students = item.students 
                break
            }
        }
        completion()
    }
    
    func fetchByGroupName(name: String, completion: @escaping () -> Void) {
        for item in ScheduleDatabase.groupsArray {
            if item.nameGroup == name {
                self.students = item.students
                break
            }
        }
        completion()
    }
    
    func fetchGroupByGroupKey(key: String) -> GroupEntity? {
        for group in ScheduleDatabase.groupsArray {
            if group.key == key {
                return group
            }
        }
        return nil
    }
}
