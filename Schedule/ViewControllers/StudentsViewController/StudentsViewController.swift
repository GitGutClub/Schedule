//
//  StudentsViewController.swift
//  Schedule
//
//  Created by MacOS on 01.06.2019.
//  Copyright © 2019 GZ. All rights reserved.
//

import UIKit

class StudentsViewController: UIViewController {
    
    private let cellID = "StudentTableViewCell"
    private var studentViewModel: StudentsViewModel = StudentsViewModel()
    var group: GroupEntity!
    var router: GroupsVCRouter!    
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var nameGroupLabel: UILabel!
    @IBOutlet var typeGroupLabel: UILabel!
    @IBOutlet var courseLabel: UILabel!
    @IBOutlet var yearLabel: UILabel!
    @IBOutlet var studentCountLabel: UILabel!
    @IBOutlet var addNewStudentButton: CustomButtonToAdd!
    
    override func viewDidLoad() {
        super.viewDidLoad()       
        
        configureUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        configureTextField()
        studentViewModel.fetchByGroupkey(key: group.key) {
            self.studentCountLabel.text = String(self.studentViewModel.students.count)
            self.tableView.reloadData()
        }
    }   

    private func configureUI() {
        self.view.backgroundColor = UIColor(hex: group.color)
        addNewStudentButton.delegate = self
        addNewStudentButton.setButtonTitle(title: "Добавить студента")
        configureBarItems()
        configureTableView()
        configureTextField()
    }
    
    private func configureTextField() {
        if let editGroup = studentViewModel.fetchGroupByGroupKey(key: group.key) {
            self.group = editGroup
        }      
        self.view.backgroundColor = UIColor(hex: group.color)
        self.yearLabel.text = String(group.year)
        self.typeGroupLabel.text = group.typeGroup
        self.courseLabel.text = "\(group.grade) курс"
        self.nameGroupLabel.text = group.nameGroup
    }
    
    private func configureTableView() {
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableView.automaticDimension
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: cellID, bundle: nil),
                           forCellReuseIdentifier: cellID)
    }
    
    private func configureBarItems() {
        router.navigationController.isNavigationBarHidden = false
        let edit = UIBarButtonItem(image: UIImage(named: "editIcon"),
                                   style: .done,
                                   target: self,
                                   action: #selector(editGroup))
        
        let delete = UIBarButtonItem(image: UIImage(named: "deleteIcon"),
                                     style: .done,
                                     target: self,
                                     action: #selector(deleteGroup))
        
        navigationItem.rightBarButtonItems = [delete, edit]
    }
    
    @objc private func editGroup() {
        router.showEditGroupScreen(withGroup: group)
    }
    
    @objc private func deleteGroup() {
        ScheduleDatabase.deleteGroup(byKey: group.key, completion: { (error) in
            guard error == nil  else {
                print("\(String(describing: error))")
                return
            }
            ScheduleDatabase.groups(completion: { (data) in
                self.router.back()
            })
        })
    }
}



extension StudentsViewController: UITableViewDataSource, UITableViewDelegate, CustomButtonDelegate {
    
    func buttonPressed() {
        router.showNewStudentScreen(byGroupKey: self.group.key)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        router.showConfigureStudentScreen(student: studentViewModel.students[indexPath.row], groupKey: self.group.key)
    }
    
    func tableView( _ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID,
                                                 for: indexPath) as! StudentTableViewCell
        
        cell.setup(with: studentViewModel.students[indexPath.row])
        
        return cell
    }    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int ) -> Int {
        return studentViewModel.students.count
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let deleteAction = UIContextualAction(style: .destructive, title: "Удалить") { (action, view, handler) in
            let deleteAction = UIAlertAction(title: "Удалить", style: .destructive, handler: { (alert) in
                ScheduleDatabase.deleteStudent(byStudentKey: self.studentViewModel.students[indexPath.row].key, groupKey: self.group.key) { (error) in
                    guard error == nil  else {
                        print("\(String(describing: error))")
                        return
                    }
                    var index = 0
                    
                    ScheduleDatabase.group(byKey: self.group.key, completion: { [weak self] (data) in
                        
                        for i in 0...ScheduleDatabase.groupsArray.count - 1 {
                            if ScheduleDatabase.groupsArray[i].key == self?.group.key {
                                index = i
                                ScheduleDatabase.groupsArray.remove(at: i)
                                break
                            }
                        }
                        
                        ScheduleDatabase.groupsArray.insert(data, at: index)
                        self?.studentViewModel.fetchByGroupkey(key: self?.group.key ?? "") {
                            self?.studentCountLabel.text = String((self?.studentViewModel.students.count)!)
                            self?.tableView.reloadData()
                        }
                    })
                }
            })
            UIAlertController.presentActionSheet(on: self, name: "Вы действительно хотите удалить студента \(self.studentViewModel.students[indexPath.row].firstName + " " + self.studentViewModel.students[indexPath.row].secondName), это действие нельзя будет отменить", buttons: [deleteAction])
        }
        
        if let cgImageX = UIImage(named: "deleteIcon")?.cgImage {
            deleteAction.image = ImageWithoutRender(cgImage: cgImageX, scale: UIScreen.main.nativeScale, orientation: .up).sd_tintedImage(with: .white)
        }
        
        deleteAction.backgroundColor = UIColor.red
        let configuration = UISwipeActionsConfiguration(actions: [deleteAction])
        configuration.performsFirstActionWithFullSwipe = false
        
        return configuration
    }
}
