//
//  StudentTableViewCell.swift
//  Schedule
//
//  Created by MacOS on 01.06.2019.
//  Copyright © 2019 GZ. All rights reserved.
//

import UIKit

class StudentTableViewCell: UITableViewCell {
    
    var student: StudentEntity?
    
    @IBOutlet var infoLabel: UILabel!
    @IBOutlet var firstNameLabel: UILabel!
    @IBOutlet var secondNameLabel: UILabel!
    @IBOutlet var studentAvatarImage: UIImageView!
    @IBOutlet var subgroupLabel: UILabel!    
    
    override func layoutSubviews() {
        studentAvatarImage.layer.cornerRadius =  10
        studentAvatarImage.layer.masksToBounds = true
        subgroupLabel.layer.borderWidth = 2
        subgroupLabel.layer.borderColor = UIColor.white.cgColor
        subgroupLabel.layer.cornerRadius = 2
        subgroupLabel.layer.masksToBounds = true
    }
    
    func setup(with student: StudentEntity) {
        studentAvatarImage.image = UIImage(named: "group")
        subgroupLabel.text = String(student.subgroup)
        setHeadman(isHeadman: student.isHeadman)
        self.student = student
        self.firstNameLabel.text = student.firstName
        self.secondNameLabel.text = student.secondName
        self.infoLabel.text = student.studentDescription
        let url = URL(string: student.profileImageURL)
        studentAvatarImage.sd_setImage(with: url)
    }
    
    private func setHeadman(isHeadman: Bool) {
        if isHeadman {
            subgroupLabel.backgroundColor = .red; subgroupLabel.textColor = .white
        } else {
            subgroupLabel.backgroundColor = .white; subgroupLabel.textColor = .black
        }
    }
}
