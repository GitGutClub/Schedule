//
//  NavigationController.swift
//  DataBase1
//
//  Created by MacOS on 30.04.2019.
//  Copyright © 2019 GZ. All rights reserved.
//

import UIKit

class NavigationController: UINavigationController {
    
    private var current: UIViewController    
    
    init() {
        self.current = StartViewController()
        super.init(nibName: nil, bundle: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        isNavigationBarHidden = true
        pushViewController(current, animated: true)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}



