//
//  Router.swift
//  Schedule
//
//  Created by MacOS on 24.06.2019.
//  Copyright © 2019 GZ. All rights reserved.
//

import Foundation
import UIKit

class BasicRouter {
    
    private(set) var navigationController: UINavigationController
   
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
        configureBackButton()
    }
    
    private func configureBackButton() {
        let imgBack = UIImage(named: "backIcon")
        self.navigationController.interactivePopGestureRecognizer?.isEnabled = false
        self.navigationController.navigationBar.backIndicatorImage = imgBack
        self.navigationController.navigationBar.backIndicatorTransitionMaskImage = imgBack        
        self.navigationController.navigationItem.leftItemsSupplementBackButton = true
        self.navigationController.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
    }
    
    func back() {
        navigationController.popViewController(animated: true)
    }
}




