//
//  File.swift
//  Schedule
//
//  Created by MacOS on 06.07.2019.
//  Copyright © 2019 GZ. All rights reserved.
//

import Foundation
import UIKit

class GlobalRouter: BasicRouter {
    
   static let shared = GlobalRouter(navigationController: NavigationController())
    
    private override init(navigationController: UINavigationController) {
        super.init(navigationController: navigationController)
    }
    
    func showMainScreen() {
        let mainVC: MainTabBarController = ViewControllersFactory.takeMainTabBarController()
        navigationController.show(mainVC, sender: nil)
    }
    
    func showInternetConnectionFailScreen() {
        let internetFailVC:  InternetConnectionFailViewController = ViewControllersFactory.takeInternetConnectionFailViewController()
        navigationController.show(internetFailVC, sender: nil)
    }
    
    func showAuthorizationScreen() {
        let authorizationVC: AuthorizationViewController = ViewControllersFactory.takeAuthorizationViewController()
        navigationController.show(authorizationVC, sender: nil)
    }
    
    func showRegistrationScreen() {
        let registrationVC: RegistrationViewController = ViewControllersFactory.takeRegistrationViewController()
        navigationController.show(registrationVC, sender: nil)
    }
    
    func showResetPasswordScreen() {
        let resetPasswordVC: ResetPasswordViewController = ViewControllersFactory.takeResetPasswordViewController()
        navigationController.show(resetPasswordVC, sender: nil)
    }
}
