//
//  LogOutVCRouter.swift
//  Schedule
//
//  Created by MacOS on 29.07.2019.
//  Copyright © 2019 GZ. All rights reserved.
//

import Foundation
import UIKit
import FirebaseAuth

class LogOutVCRouter: BasicRouter {
   
    init() {
        let vc: UIViewController = LogOutVC()
        super.init(navigationController: UINavigationController(rootViewController: vc))
        vc.tabBarItem = UITabBarItem(title:  "LogOut", image: UIImage(named: "businessman"), tag: 2)
    }
}

class LogOutVC: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        do {
            try Auth.auth().signOut()
            GlobalRouter.shared.showAuthorizationScreen()
        } catch {
            print(error)
        }
    }
}
