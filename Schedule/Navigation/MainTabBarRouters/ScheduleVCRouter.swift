//
//  ScheduleVCRouter.swift
//  Schedule
//
//  Created by MacOS on 06.07.2019.
//  Copyright © 2019 GZ. All rights reserved.
//

import Foundation
import UIKit

class ScheduleVCRouter: BasicRouter {
    // Ротуер экрана расспмсания
    func showCreateNewScheduleScreen() {
        let creatNewScheduleVC: CreateNewScheduleViewController = ViewControllersFactory.takeCreateNewScheduleViewController()
        creatNewScheduleVC.router = self
        creatNewScheduleVC.hidesBottomBarWhenPushed = true
        navigationController.pushViewController(creatNewScheduleVC, animated: true)
    }
    
    func showEditScheduleScreen(schedule: ScheduleEntity) {
        let editScheduleVC: EditScheduleViewController = ViewControllersFactory.takeEditScheduleViewController()
        editScheduleVC.router = self
        editScheduleVC.targetSchedule = schedule
        editScheduleVC.hidesBottomBarWhenPushed = true
        navigationController.pushViewController(editScheduleVC, animated: true)
    }
    
    func showAttendanceScreen(nameGroup: String, schedule: ScheduleEntity, isNew: Bool) {
        let attendanceVC: AttendanceViewController = ViewControllersFactory.takeAttendanceViewController()
        attendanceVC.router = self
        attendanceVC.isNew = isNew
        attendanceVC.nameGroup = nameGroup
        attendanceVC.schedule = schedule
        attendanceVC.hidesBottomBarWhenPushed = true
        navigationController.pushViewController(attendanceVC, animated: true)
    }
    
    init() {
        // Создаю экран и кладу в него ротуер(Самого себя)
        let vc: ScheduleViewController = ViewControllersFactory.takeScheduleViewController()
        super.init(navigationController: UINavigationController(rootViewController: vc))
        vc.router = self        
        vc.tabBarItem = UITabBarItem (
            title: "Раcписание",
            image: UIImage(named: "calendar")?.withRenderingMode(
                UIImage.RenderingMode.alwaysTemplate
            ),
            tag: 0
        )
    }
}
