//
//  GroupsVCRouter.swift
//  Schedule
//
//  Created by MacOS on 06.07.2019.
//  Copyright © 2019 GZ. All rights reserved.
//

import Foundation
import UIKit

class GroupsVCRouter: BasicRouter {
    // Роутер экрана групп
    func showCreateNewGroupScreen() {
        let createNewGroupVC: ConfigureGroupViewController = ViewControllersFactory.takeConfigureGroupViewController()
        createNewGroupVC.router = self
        createNewGroupVC.hidesBottomBarWhenPushed = true
        navigationController.pushViewController(createNewGroupVC, animated: true)
    }
    
    func showEditGroupScreen(withGroup group: GroupEntity) {
        let editGroupVC: ConfigureGroupViewController = ViewControllersFactory.takeConfigureGroupViewController()
        editGroupVC.router = self
        editGroupVC.hidesBottomBarWhenPushed = true
        editGroupVC.group = group
        navigationController.pushViewController(editGroupVC, animated: true)
    }
    
    func showStudentsScreen(group: GroupEntity) {
        let studentsVC:  StudentsViewController = ViewControllersFactory.takeStudentsViewController()
        studentsVC.router = self
        studentsVC.group = group
        studentsVC.hidesBottomBarWhenPushed = true
        navigationController.pushViewController(studentsVC, animated: true)
    }
    
    func showNewStudentScreen(byGroupKey key: String) {
        let newStudentsVC: ConfigureStudentViewController = ViewControllersFactory.takeConfigureStudentViewController()
        newStudentsVC.router = self
        newStudentsVC.groupKey = key
        newStudentsVC.hidesBottomBarWhenPushed = true
        navigationController.pushViewController(newStudentsVC, animated: true)
    }
    
    func showConfigureStudentScreen(student: StudentEntity, groupKey: String) {
        let editStudentVC: ConfigureStudentViewController = ViewControllersFactory.takeConfigureStudentViewController()
        editStudentVC.router = self
        editStudentVC.student = student
        editStudentVC.groupKey = groupKey
        editStudentVC.hidesBottomBarWhenPushed = true
        navigationController.pushViewController(editStudentVC, animated: true)
    }
    
    init() {
        let vc: GroupsViewController = ViewControllersFactory.takeGroupsViewController()
        super.init(navigationController: UINavigationController(rootViewController: vc))
        vc.router = self
        vc.tabBarItem = UITabBarItem(
            title: "Группы",
            image: UIImage(named: "user_group_man_woman")?.withRenderingMode(
                UIImage.RenderingMode.alwaysTemplate
            ),
            tag: 1
        )
    }
    
}
